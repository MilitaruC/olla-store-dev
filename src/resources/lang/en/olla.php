<?php

return [
    'Change flag' => 'Change flag',
    'Dashboard' => 'Dashboard',
    'Admin' => 'Admin',
    'Admins' => 'Admins',
    'Customer' => 'Customer',
    'Customers' => 'Customers',
    'Order' => 'Order',
    'Orders' => 'Orders',
    'Category' => 'Category',
    'Categories' => 'Categories',
    'Product' => 'Product',
    'Products' => 'Products',
    'Stores' => 'Stores',
    'Store' => 'Store',
];
