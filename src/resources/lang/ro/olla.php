<?php

return [
    'Change flag' => 'Schimba drapelul',
    'Dashboard' => 'Dashboard',
    'Admin' => 'Administrator',
    'Admins' => 'Administratori',
    'Customer' => 'Client',
    'Customers' => 'Clienti',
    'Order' => 'Comanda',
    'Orders' => 'Comenzi',
    'Category' => 'Categorie',
    'Categories' => 'Categorii',
    'Product' => 'Produs',
    'Products' => 'Produse',
    'Stores' => 'Magazine',
    'Store' => 'Magazin',
];
