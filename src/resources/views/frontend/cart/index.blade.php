@extends('os_views::frontend.layouts.default')

@section('title', 'Cart')

@section('content')

    {{ pageTitle('Cart') }}

    <div class="row">
        <div class="col-md-9">
            <table id="products-list" class="table">
                <thead>
                <tr>
                    <th style="width: 110px;"></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="cartItemsList">
                @foreach($cartData->userCart as $cartItem)
                    <tr>
                        <td>
                            <a href="{{ url('/p/'. $cartItem->product->url) }}">
                                @if(count($cartItem->product->images))
                                    <img src="{{ url($cartItem->product->images[0]->rel_path . 'thumbnail/100x100-' . $cartItem->product->images[0]->file) }}" class="img-fluid">
                                @else
                                    <img src="{{ url('/media/test.webp') }}" class="img-fluid">
                                @endif
                            </a>
                        </td>
                        <td>
                            <div class="product-item-name">
                                <a href="{{ url('/p/'. $cartItem->product->url) }}"><h5>{{ $cartItem->product->full_name }}</h5></a>
                            </div>
                        </td>
                        <td>
                            <input type="number" value="{{ $cartItem->product_qty }}" class="cartQty form-control" data-pid="{{ $cartItem->product_id }}">
                        </td>
                        <td>
                            <div class="product-item-price">
                                <span class="p-price cartProductTotalPrice-{{ $cartItem->product_id }}">{{ number_format(($cartItem->product->price * $cartItem->product_qty), 2) }}</span> <span class="p-currency">EUR</span>
                            </div>
                        </td>
                        <td>
                            <i class="fa fa-trash" style="color: red" onclick="removeItemFromCart({{ $cartItem->product_id }})"></i>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-md-3">
            <h3>Order Summary</h3>
            <table>
                <tbody>
                <tr>
                    <td>Product price:</td>
                    <td><span class="cartProductsPrice">{{ number_format($cartData->productsPrice, '2') }}</span> EUR</td>
                </tr>
                <tr>
                    <td>Delivery price:</td>
                    <td><span class="cartDeliveryPrice">{{ number_format($cartData->deliveryPrice, '2') }}</span> EUR</td>
                </tr>
                </tbody>
            </table>
            <hr>
            <h3>Total</h3>
            <p><span class="cartTotalPrice">{{ number_format($cartData->totalPrice, '2') }}</span> EUR</p>
            <hr>
            <button class="form-control btn btn-outline-info" onclick="window.location='/cart/checkout'">Continue</button>
            <br><br><br>
        </div>
    </div>

@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $(document).on('change', '.cartQty', function (e) {
                updateCart($(this).attr('data-pid'), $(this).val())
            });
        });

        function addToCart(productID, qty = 1) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/addToCart/' + productID + '/' + qty,
                method: 'get',
                data: {},
                success: function (data) {
                    $('#productsQty').text('( ' + data.cartTotalQty + ' )');
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function removeItemFromCart(productID) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/removeItemFromCart/' + productID,
                method: 'get',
                data: {},
                success: function (data) {
                    $('#productsQty').text('( ' + data.cartTotalQty + ' )');
                    $('.cartProductsPrice').text(parseFloat(data.cartData.productsPrice).toFixed(2));
                    $('.cartDeliveryPrice').text(parseFloat(data.cartData.deliveryPrice).toFixed(2));
                    $('.cartTotalPrice').text(parseFloat(data.cartData.totalPrice).toFixed(2));
                    loadCartItemsAjax();
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function loadCartItemsAjax() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/loadCartItemsAjax',
                method: 'get',
                data: {},
                success: function (data) {
                    $('#cartItemsList').html(data.cartItemsView);
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function updateCart(productID, qty = 1) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/updateCart/' + productID + '/' + qty,
                method: 'get',
                data: {},
                success: function (data) {
                    $('#productsQty').text('( ' + data.cartTotalQty + ' )');
                    Object.entries(data.userCart).forEach(entry => {
                        const [key, value] = entry;
                        var pTPrice = value.product_qty * value.product.price;
                        $('.cartProductTotalPrice-' + value.product.id).text(parseFloat(pTPrice).toFixed(2));
                    });
                    $('.cartProductsPrice').text(parseFloat(data.cartData.productsPrice).toFixed(2));
                    $('.cartDeliveryPrice').text(parseFloat(data.cartData.deliveryPrice).toFixed(2));
                    $('.cartTotalPrice').text(parseFloat(data.cartData.totalPrice).toFixed(2));
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }
    </script>
@endpush
