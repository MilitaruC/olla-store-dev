@foreach($cartData->userCart as $cartItem)
    <tr>
        <td>
            <a href="{{ url('/p/'. $cartItem->product->url) }}">
                @if(count($cartItem->product->images))
                    <img src="{{ url($cartItem->product->images[0]->rel_path . 'thumbnail/100x100-' . $cartItem->product->images[0]->file) }}" class="img-fluid">
                @else
                    <img src="{{ url('/media/test.webp') }}" class="img-fluid">
                @endif
            </a>
        </td>
        <td>
            <div class="product-item-name">
                <a href="{{ url('/p/'. $cartItem->product->url) }}"><h5>{{ $cartItem->product->full_name }}</h5></a>
            </div>
        </td>
        <td>
            <input type="number" value="{{ $cartItem->product_qty }}" class="cartQty form-control" data-pid="{{ $cartItem->product_id }}">
        </td>
        <td>
            <div class="product-item-price">
                <span class="p-price cartProductTotalPrice-{{ $cartItem->product_id }}">{{ number_format(($cartItem->product->price * $cartItem->product_qty), 2) }}</span> <span class="p-currency">EUR</span>
            </div>
        </td>
        <td>
            <i class="fa fa-trash" style="color: red" onclick="removeItemFromCart({{ $cartItem->product_id }})"></i>
        </td>
    </tr>
@endforeach
