@extends('os_views::frontend.layouts.default')

@section('title', 'Order Details')

@section('content')

    {{ pageTitle('Order Details') }}

    <div class="row">
        <div class="col-md-9">
            <div class="invoiceAddress">
                <h5>Invoice Address</h5>
                <hr>
                <select name="invoice_address" id="invoice_address" class="form-control">
                    <option value="person">Person</option>
                    <option value="company">Company</option>
                </select>
                <div id="invoice_address_person" class="row">
                    <div class="col-md-6 mb-3">
                        <label for="invoice_address_name" class="form-label">First & Last Name</label>
                        <input type="text" class="form-control" name="invoice_address_name" id="invoice_address_name" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="invoice_address_phone" class="form-label">Phone</label>
                        <input type="text" class="form-control" name="invoice_address_phone" id="invoice_address_phone" required>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="invoice_address_county" class="form-label">County</label>
                        <select name="invoice_address_county" id="invoice_address_county" class="form-control">
                            <option value="0" selected="">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="invoice_address_locality" class="form-label">Locality</label>
                        <select name="invoice_address_locality" id="invoice_address_locality" class="form-control">
                            <option value="0" selected="">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="invoice_address_details" class="form-label">Address Details</label>
                        <textarea type="text" class="form-control" name="invoice_address_details" id="invoice_address_details" required></textarea>
                    </div>
                </div>
                <div id="invoice_address_company" class="row">
                    <div class="col-md-6 mb-3">
                        <label for="invoice_address_company_name" class="form-label">Company Name</label>
                        <input type="text" class="form-control" name="invoice_address_company_name" id="invoice_address_company_name" required>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="invoice_address_company_vat_code" class="form-label">Registration Code</label>
                        <div class="form-inline row">
                            <div class="col-md-4">
                                <select name="invoice_address_company_vat_code" id="invoice_address_company_vat_code" class="form-control form-inline">
                                <option value="RO" selected="">RO</option>
                                <option value="">---</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-inline" name="invoice_address_company_code" id="invoice_address_company_code" required>
                            </div>
                        </div>

                    </div>
                    <div class="mb-3 col-md-12">
                        <label for="invoice_address_j" class="form-label">Registration Number</label>
                        <div class="form-inline row">
                            <div class="col-md-3">
                                <select name="invoice_address_j" id="invoice_address_j" class="form-control">
                                    <option value="J" selected="">J</option>
                                    <option value="F">F</option>
                                    <option value="C">C</option>
                                    <option value="">-</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="invoice_address_j1" id="invoice_address_j1" class="form-control">
                                    <option value="">--</option>
                                    <option value="0">&nbsp;&nbsp;00&nbsp;&nbsp;</option>
                                    <option value="1">&nbsp;&nbsp;01&nbsp;&nbsp;</option>
                                    <option value="2">&nbsp;&nbsp;02&nbsp;&nbsp;</option>
                                    <option value="3">&nbsp;&nbsp;03&nbsp;&nbsp;</option>
                                    <option value="4">&nbsp;&nbsp;04&nbsp;&nbsp;</option>
                                    <option value="5">&nbsp;&nbsp;05&nbsp;&nbsp;</option>
                                    <option value="6">&nbsp;&nbsp;06&nbsp;&nbsp;</option>
                                    <option value="7">&nbsp;&nbsp;07&nbsp;&nbsp;</option>
                                    <option value="8">&nbsp;&nbsp;08&nbsp;&nbsp;</option>
                                    <option value="9">&nbsp;&nbsp;09&nbsp;&nbsp;</option>
                                    <option value="10">&nbsp;&nbsp;10&nbsp;&nbsp;</option>
                                    <option value="11">&nbsp;&nbsp;11&nbsp;&nbsp;</option>
                                    <option value="12">&nbsp;&nbsp;12&nbsp;&nbsp;</option>
                                    <option value="13">&nbsp;&nbsp;13&nbsp;&nbsp;</option>
                                    <option value="14">&nbsp;&nbsp;14&nbsp;&nbsp;</option>
                                    <option value="15">&nbsp;&nbsp;15&nbsp;&nbsp;</option>
                                    <option value="16">&nbsp;&nbsp;16&nbsp;&nbsp;</option>
                                    <option value="17">&nbsp;&nbsp;17&nbsp;&nbsp;</option>
                                    <option value="18">&nbsp;&nbsp;18&nbsp;&nbsp;</option>
                                    <option value="19">&nbsp;&nbsp;19&nbsp;&nbsp;</option>
                                    <option value="20">&nbsp;&nbsp;20&nbsp;&nbsp;</option>
                                    <option value="21">&nbsp;&nbsp;21&nbsp;&nbsp;</option>
                                    <option value="22">&nbsp;&nbsp;22&nbsp;&nbsp;</option>
                                    <option value="23">&nbsp;&nbsp;23&nbsp;&nbsp;</option>
                                    <option value="24">&nbsp;&nbsp;24&nbsp;&nbsp;</option>
                                    <option value="25">&nbsp;&nbsp;25&nbsp;&nbsp;</option>
                                    <option value="26">&nbsp;&nbsp;26&nbsp;&nbsp;</option>
                                    <option value="27">&nbsp;&nbsp;27&nbsp;&nbsp;</option>
                                    <option value="28">&nbsp;&nbsp;28&nbsp;&nbsp;</option>
                                    <option value="29">&nbsp;&nbsp;29&nbsp;&nbsp;</option>
                                    <option value="30">&nbsp;&nbsp;30&nbsp;&nbsp;</option>
                                    <option value="31">&nbsp;&nbsp;31&nbsp;&nbsp;</option>
                                    <option value="32">&nbsp;&nbsp;32&nbsp;&nbsp;</option>
                                    <option value="33">&nbsp;&nbsp;33&nbsp;&nbsp;</option>
                                    <option value="34">&nbsp;&nbsp;34&nbsp;&nbsp;</option>
                                    <option value="35">&nbsp;&nbsp;35&nbsp;&nbsp;</option>
                                    <option value="36">&nbsp;&nbsp;36&nbsp;&nbsp;</option>
                                    <option value="37">&nbsp;&nbsp;37&nbsp;&nbsp;</option>
                                    <option value="38">&nbsp;&nbsp;38&nbsp;&nbsp;</option>
                                    <option value="39">&nbsp;&nbsp;39&nbsp;&nbsp;</option>
                                    <option value="40">&nbsp;&nbsp;40&nbsp;&nbsp;</option>
                                    <option value="41">&nbsp;&nbsp;41&nbsp;&nbsp;</option>
                                    <option value="42">&nbsp;&nbsp;42&nbsp;&nbsp;</option>
                                    <option value="43">&nbsp;&nbsp;43&nbsp;&nbsp;</option>
                                    <option value="44">&nbsp;&nbsp;44&nbsp;&nbsp;</option>
                                    <option value="50">&nbsp;&nbsp;50&nbsp;&nbsp;</option>
                                    <option value="51">&nbsp;&nbsp;51&nbsp;&nbsp;</option>
                                    <option value="52">&nbsp;&nbsp;52&nbsp;&nbsp;</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="invoice_address_j2" id="invoice_address_j2" required>
                            </div>
                            <div class="col-md-2">
                                <select name="invoice_address_j3" id="invoice_address_j3" class="form-control">
                                    <option value="">--</option>
                                    @for($i = date('Y'); $i >= 1990; $i--)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="invoice_address_bank_name" class="form-label">Bank Name</label>
                        <input type="text" class="form-control" name="invoice_address_bank_name" id="invoice_address_bank_name" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="invoice_address_bank_account" class="form-label">Bank Account</label>
                        <input type="text" class="form-control" name="invoice_address_bank_account" id="invoice_address_bank_account" required>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="invoice_address_county" class="form-label">County</label>
                        <select name="invoice_address_county" id="invoice_address_county" class="form-control">
                            <option value="0" selected="">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="invoice_address_locality" class="form-label">Locality</label>
                        <select name="invoice_address_locality" id="invoice_address_locality" class="form-control">
                            <option value="0" selected="">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="invoice_address_details" class="form-label">Address Details</label>
                        <textarea type="text" class="form-control" name="invoice_address_details" id="invoice_address_details" required></textarea>
                    </div>
                </div>
            </div>
            <div class="deliveryOptions">
                <h5>Delivery Method</h5>
                <hr>
                <select name="delivery_method" id="delivery" class="form-control">
                    <option value="courier">Courier</option>
                    <option value="personal-lifting">Personal Lifting</option>
                </select>
            </div>
            <div class="deliveryAddress">
                <h5>Delivery Address</h5>
                <hr>

            </div>

            <div class="paymentMethod">
                <h5>Payment Method</h5>
                <hr>

            </div>
        </div>
        <div class="col-md-3">
            <h3>Order Summary</h3>
            <table>
                <tbody>
                <tr>
                    <td>Product price: </td>
                    <td><span class="cartProductsPrice">{{ number_format($cartData->productsPrice, '2') }}</span> EUR</td>
                </tr>
                <tr>
                    <td>Delivery price: </td>
                    <td><span class="cartDeliveryPrice">{{ number_format($cartData->deliveryPrice, '2') }}</span> EUR</td>
                </tr>
                </tbody>
            </table>
            <hr>
            <h3>Total</h3>
            <p><span class="cartTotalPrice">{{ number_format($cartData->totalPrice, '2') }}</span> EUR</p>
            <hr>
            <button class="form-control btn btn-outline-info">Continue</button>
            <br><br><br>
        </div>
    </div>

@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(document).on('change', '.cartQty', function (e) {
                updateCart($(this).attr('data-pid'), $(this).val())
            });
        });

        function addToCart(productID, qty = 1)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/addToCart/' + productID + '/' + qty,
                method: 'get',
                data: {},
                success: function (data) {
                    $('#productsQty').text('( ' + data.cartTotalQty + ' )');
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function updateCart(productID, qty = 1)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/updateCart/' + productID + '/' + qty,
                method: 'get',
                data: {},
                success: function (data) {
                    $('#productsQty').text('( ' + data.cartTotalQty + ' )');
                    Object.entries(data.userCart).forEach(entry => {
                        const [key, value] = entry;
                        var pTPrice = value.product_qty * value.product.price;
                        $('.cartProductTotalPrice-' + value.product.id).text(parseFloat(pTPrice).toFixed(2));
                    });
                    $('.cartProductsPrice').text(parseFloat(data.cartData.productsPrice).toFixed(2));
                    $('.cartDeliveryPrice').text(parseFloat(data.cartData.deliveryPrice).toFixed(2));
                    $('.cartTotalPrice').text(parseFloat(data.cartData.totalPrice).toFixed(2));
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }
    </script>
@endpush
