<div class="col-md-12">
    @if(count($products)) {{ $products->links()}} @endif</div>
<div class="row products-list">
    @foreach($products as $product)
        <div class="col-md-4 product-item">
            <div class="product-item-wrapper">
                <div class="product-action-favorite"><i class="fa-regular fa-heart"></i></div>
                <div class="product-action-compare"><i class="fa-regular fa-square"></i></div>
                <div class="product-item-image">
                    <a href="{{ url('/p/'. $product->url) }}">
                        @if(count($product->images))
                            <img src="{{ url($product->images[0]->rel_path . 'thumbnail/310x310-' . $product->images[0]->file) }}" class="img-fluid">
                        @else
                            <img src="{{ url('/media/test.webp') }}" class="img-fluid">
                        @endif
                    </a>
                </div>
                <div class="product-item-details">
                    <div class="product-item-name">
                        <a href="{{ url('/p/'. $product->url) }}"><h2>{{ $product->name }}</h2></a>
                    </div>
                    <div class="product-item-price">
                        <span class="p-price">{{ number_format($product->price, 2) }}</span> <span class="p-currency">EUR</span>
                    </div>
                    <div class="product-item-cart-action">
                        <button class="btn btn-sm btn-warning btn-add-to-cart form-control" onclick="addToCart({{ $product->id }});"><i class="fa-solid fa-cart-shopping"></i> Add To Cart</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="col-md-12">@if(count($products)) {{ $products->links()}} @endif</div>
