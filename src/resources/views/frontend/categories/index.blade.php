@extends('os_views::frontend.layouts.default')

@section('title', $category->name)

@section('content')

{{--    @include('os_views::frontend.components.slider')--}}

    {{ pageTitle($category->name) }}

    <div class="breadcrumb">
        <ul>
            @foreach($breadcrumbs as $k=>$breadcrumb)
                <li class="breadcrumbItem"><a href="{{ url('/c/'.$breadcrumb->url) }}">{{ $breadcrumb->name }}</a></li>
                @if(count($breadcrumbs) > ($k + 1)) <li class="breadcrumbDelimiter">/</li> @endif
            @endforeach
        </ul>
    </div>

    <div class="row">
        <div class="col-md-3">
            @foreach($attributes as $attribute)
                <div class="attribute-card">
                    <h3 class="attribute-name">{{ $attribute->name }}</h3>
                    @foreach($attribute->attributeValues as $attributeValue)
                        <div class="form-check attribute-value">
                            <input type="checkbox" class="form-check-input attr-value" data-attribute-id="{{ $attribute->id }}" data-attribute-value-id="{{ $attributeValue->id }}"
                            @if(in_array($attributeValue->id, ($request->attributeValues ?? []))) checked="checked" @endif>
                            <label class="form-check-label">{{ $attributeValue->value }}
                                <span class="attribute-value-products-count">({{ $attributeValue->products_count }})</span></label>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div id="products-list" class="col-md-9">
            <div class="col-md-12">@if(count($products)) {{ $products->links()}} @endif</div>
            <div class="row products-list">
                @foreach($products as $product)
                    <div class="col-md-4 product-item">
                        <div class="product-item-wrapper">
                            <div class="product-action-favorite"><i class="fa-regular fa-heart"></i></div>
                            <div class="product-action-compare"><i class="fa-regular fa-square"></i></div>
                            <div class="product-item-image">
                                <a href="{{ url('/p/'. $product->url) }}">
                                    @if(count($product->images))
                                        <img src="{{ url($product->images[0]->rel_path . 'thumbnail/310x310-' . $product->images[0]->file) }}" class="img-fluid">
                                    @else
                                        <img src="{{ url('/media/test.webp') }}" class="img-fluid">
                                    @endif
                                </a>
                            </div>
                            <div class="product-item-details">
                                <div class="product-item-name">
                                    <a href="{{ url('/p/'. $product->url) }}"><h2>{{ $product->name }}</h2></a>
                                </div>
                                <div class="product-item-price">
                                    <span class="p-price">{{ number_format($product->price, 2) }}</span> <span class="p-currency">EUR</span>
                                </div>
                                <div class="product-item-cart-action">
                                    <button class="btn btn-sm btn-warning btn-add-to-cart form-control" onclick="addToCart({{ $product->id }});"><i class="fa-solid fa-cart-shopping"></i> Add To Cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-12">@if(count($products)) {{ $products->links()}} @endif</div>
        </div>
    </div>

@endsection
@push('after-scripts')
<script>
let model = {
    name: 'product',
    names: 'products',
    Name: 'Product',
    Names: 'Products',
    urlSearch: '/c/{{ $category->url }}',
    sendData: {
        perPage: 10,
        page: '1',
        sortBy: 'id',
        sortOrder: 'DESC',
        search: '',
        attributeValues: [],
        activeStatus: null,
        storeID: null,
    }
};
$(document).ready(function() {
    $(document).on('click', '.pagination a', function(e) {
        e.preventDefault();
        let url = new URL($(this).attr('href'));
        let page = url.searchParams.get('page');
        model.sendData.page = page;
        filterModel();
    });

    $(document).on('click', '.attr-value', function (e) {
        model.sendData.page = '1';
        model.sendData.attributeValues = [];
        var filters = $('.attr-value');
        Object.keys(filters).forEach(function(key) {
            if($(filters[key]).is(':checked')){
                model.sendData.attributeValues.push($(filters[key]).attr('data-attribute-value-id'));
            }
        });
        filterModel();
    });
});

function filterModel(page = null)
{
    if(page) model.sendData.page = page;
    model.sendData.search = $('#search').val();
    model.sendData.perPage = $('#perPage').val() || 6;
    model.sendData.sortBy = $('#sortBy').val();
    model.sendData.sortOrder = $('#sortOrder').val();
    var urlParameters = {};
    Object.keys(model.sendData).forEach(function(key) {
        if(model.sendData[key]){
            urlParameters[key] = model.sendData[key];
        }
    });
    window.history.pushState("", "", model.urlSearch+'?'+toQueryString(urlParameters)/*.slice(0, -2)*/);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: model.urlSearch,
        method: 'post',
        data: model.sendData,
        success: function (data) {
            $('#'+model.names+'-list').html(data);
        },
        error: function (data) {
            alert('Error!');
        }
    });
}

function addToCart(productID, qty = 1)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: '/addToCart/' + productID + '/' + qty,
        method: 'get',
        data: {},
        success: function (data) {
            $('#productsQty').text('( ' + data.cartTotalQty + ' )');
        },
        error: function (data) {
            alert('Error!');
        }
    });
}

function removeItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

function removeItemAll(arr, value) {
    var i = 0;
    while (i < arr.length) {
        if (arr[i] === value) {
            arr.splice(i, 1);
        } else {
            ++i;
        }
    }
    return arr;
}

function toQueryString(obj, prefix) {
    var str = [], k, v;
    for(var p in obj) {
        if (!obj.hasOwnProperty(p)) {continue;}
        if (~p.indexOf('[')) {
            k = prefix ? prefix + "[" + p.substring(0, p.indexOf('[')) + "]" + p.substring(p.indexOf('[')) : p;
        } else {
            k = prefix ? prefix + "[" + p + "]" : p;
        }
        v = obj[p];
        str.push(typeof v == "object" ?
            toQueryString(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
}
</script>
@endpush
@push('after-styles')
<style>
.content{
    min-height: 90vh;
    padding-bottom: 40px;
}
.page-link, .page-link:hover{
    color: #EF7427;
}
.page-item.active .page-link{
    background-color: #EF7427;
    border-color: #EF7427;
}
.breadcrumb{

}
.breadcrumb ul{
    text-decoration: none;
    padding: 0px;
    margin: 0px;
}
.breadcrumb ul li{
    text-decoration: none;
    display: inline;
    padding: 5px 5px 5px 0px;
}
.breadcrumbDelimiter{
    font-weight: bold;
}
.attribute-card{
    position: relative;
    background: #fff;
    padding: 5px;
    margin-bottom: 10px;
    border-radius: 4px;
    box-sizing: border-box;
}
.attribute-name{
    background: #EF7427;
    color: #fff;
    font-size: 20px;
    padding: 5px;
}
.attribute-value{
    margin-left: 10px !important;
}
.attribute-value-products-count{
    color: #888888;
}
.products-list{
    margin-bottom: 20px;
}
.product-item{
    padding: 5px;
    box-sizing: border-box;
}
.product-item-wrapper{
    position: relative;
    background: #fff;
    padding: 5px;
    padding-bottom: 25px;
    border-radius: 4px;
    box-sizing: border-box;
}
.product-action-favorite, .product-action-compare{
    position: absolute;
    width: 30px;
    height: 30px;
    border-radius: 15px;
    background: #fff;
    color: #EF7427;
    box-sizing: border-box;
    /*border: 1px solid #EF7427;*/
    vertical-align: center;
    text-align: center;
    font-size: 20px;
    line-height: 30px;
    cursor: pointer;
}
.product-action-favorite i, .product-action-compare i{
    font-size: 20px;
    line-height: 30px;
}
.product-action-favorite{
    top: 10px;
    right: 10px;
}
.product-action-compare{
    top: 45px;
    right: 10px;
}
.product-item-image{
    height: 260px;
    border: 1px solid #EF7427;
    margin-bottom: 10px;
    box-sizing: border-box;
    text-align: center;
}
.product-item-image a img{
    height: 256px;
}
.product-item-details{
    text-align: center;
    box-sizing: border-box;
    padding: 0px 20px;
}
.product-item-name{
    min-height: 58px;
}
.product-item-name a h2{
    font-size: 16px;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
}
.product-item-price{
    text-align: left;
    color: #EF7427;
}
.product-item-cart-action{
    padding: 10px 0px;
}
.btn-add-to-cart{
    background: #EF7427;
    background-color: #EF7427 !important;
    color: #fff !important;
}
</style>
@endpush
