@extends('os_views::frontend.layouts.default')

@section('title', $product->name)

@section('content')

{{--    @include('os_views::frontend.components.slider')--}}

    {{ pageTitle($product->full_name) }}

    {{--<div class="breadcrumb">
        <ul>
            @foreach($breadcrumbs as $k=>$breadcrumb)
                <li class="breadcrumbItem"><a href="{{ url('/c/'.$breadcrumb->url) }}">{{ $breadcrumb->name }}</a></li>
                @if(count($breadcrumbs) > ($k + 1)) <li class="breadcrumbDelimiter">/</li> @endif
            @endforeach
        </ul>
    </div>--}}

    <div class="row">
        <div id="product-gallery" class="col-md-6">
            <!-- Add images to <div class="fotorama"></div> -->
            <div class="fotorama">
                @foreach($product->images as $image)
                    <img src="{{ asset( $image->ProductImagePath ) }}">
                @endforeach
            </div>
        </div>
        <div id="product-details" class="col-md-6">
            <div class="product-item-price">
                <span class="p-price">{{ number_format($product->price, 2) }}</span> <span class="p-currency">EUR</span>
            </div>
            <div class="product-item-cart-action">
                <button class="btn btn-sm btn-warning btn-add-to-cart form-control" onclick="addToCart({{ $product->id }});"><i class="fa-solid fa-cart-shopping"></i> Add To Cart</button>
            </div>
        </div>
        <div class="col-md-12">
            <h4 class="product-description">Description</h4>
            {!! $product->description !!}
        </div>
    </div>

@endsection
@push('after-scripts')
    <script src="{{ asset('os_assets/admin/vendor/ckeditor/ckeditor.js') }}"></script>
<link  href="{{ asset('os_assets/vendor/fotorama-4.6.4/fotorama.css') }}" rel="stylesheet">
<script src="{{ asset('os_assets/vendor/fotorama-4.6.4/fotorama.js') }}"></script>
<script>
$(document).ready(function() {

});

function addToCart(productID, qty = 1)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: '/addToCart/' + productID + '/' + qty,
        method: 'get',
        data: {},
        success: function (data) {
            $('#productsQty').text('( ' + data.cartTotalQty + ' )');
        },
        error: function (data) {
            alert('Error!');
        }
    });
}
</script>
@endpush
@push('after-styles')
<style>
.content{
    min-height: 90vh;
    padding-bottom: 40px;
}
.breadcrumb{

}
.breadcrumb ul{
    text-decoration: none;
    padding: 0px;
    margin: 0px;
}
.breadcrumb ul li{
    text-decoration: none;
    display: inline;
    padding: 5px 5px 5px 0px;
}
.breadcrumbDelimiter{
    font-weight: bold;
}
.product-title{
    padding: 40px 0px 10px 0px;
}
.product-description{
    text-align: center;
}
.product-item-cart-action{
    padding: 10px 0px;
}
.btn-add-to-cart{
    background: #EF7427;
    background-color: #EF7427 !important;
    color: #fff !important;
}
</style>
@endpush
