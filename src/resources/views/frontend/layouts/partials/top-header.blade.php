<div class="top-navbar">
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="tel:0732792278">
                    <span>0732 792 278</span>
                </a>&nbsp;&nbsp;
                <a href="mailto:contact@easycart.ro">
                    <span>contact@easycart.ro</span>
                </a>
            </div>
            <div class="col text-right">
                <span>Livrare gratuita pentru comenzile de peste 350 LEI</span>
            </div>
        </div>
    </div>
</div>
