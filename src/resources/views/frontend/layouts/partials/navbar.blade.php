<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="{{ asset('os_assets/images/demo-logo.png') }}" height="50"></a>
        <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-dark" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
        </form>
        <div class="d-flex">
            <a href="{{ url('/cart') }}" class="btn btn-outline-success me-2"><i class="fa-solid fa-cart-shopping"></i>
                <span id="productsQty">
                    @if($cartData ?? null)
                        @if(($cartData->cartTotalQty ?? 0) > 0)
                            ( {{ $cartData->cartTotalQty }} )
                        @endif
                    @endif
                </span>
            </a>
            @if(Auth::check())
                <a href="{{ url('/my-account') }}" class="btn btn-outline-warning me-2"><i class="fa-solid fa-user"></i>
                    {{ auth()->user()->name }}</a>
                <a href="{{ url('/logout') }}" class="btn btn-outline-info">Logout</a>
            @else
                <a href="{{ url('/login') }}" class="btn btn-outline-info">Login</a>
            @endif
        </div>
    </div>
</nav>
<style>
    .categoriesWrapper{
        width: 200px;
    }
    .navbarBars{
        padding-left: 100px;
    }
    #navbarContent{
        padding:0px;
        border-radius: 0px;
        border-color: transparent;
        width: 200px;
        position:absolute;
        left:-1px;
        top:35px;
    }
    .lvl0{
        padding: 0px;
        margin: 0px;
        text-decoration: none;
        list-style: none;
        overflow: visible;
    }
    .parent-category{
        padding: 5px 15px;
        position: relative;
    }
    .parent-category-child{
        padding: 10px;
        margin: 0px;
        text-decoration: none;
        list-style: none;
        position: absolute;
        left: 196px;
        top: 0px;
        min-width: 300px;
        background: #ffffff;
        display: none;
    }
    .parent-category-child-child{
        padding: 10px;
        margin: 0px;
        text-decoration: none;
        list-style: none;
        position: absolute;
        left: 296px;
        top: 0px;
        min-width: 300px;
        background: #ffffff;
        display: none;
    }
    .parent-category:hover{
        background: red;
    }
    .parent-category:hover .parent-category-child{
        display: block;
    }
    .parent-category-child:hover .parent-category-child-child{
        display: block;
    }
</style>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown" id="categoriesWrapper">
                    <a class="nav-link dropdown bg-black" href="#" id="navbarMenu" role="button" data-bs-toggle="dropdown" aria-expanded="false">Products <span class="navbarBars"><i class="fa-solid fa-bars"></i></span></a>
                    <div id="navbarContent" class="dropdown-menu" aria-labelledby="navbarMenu">
                        <ul class="lvl0">
                        @foreach(hlp()::categoryTree() as $cat)
                           <li class="parent-category">
                               <a href="{{ url('/c/'.$cat->url) }}">{{ $cat->name }}</a>
                               @if(count($cat->children))
                                   <ul class="lvl1 parent-category-child">
                                   @foreach($cat->children as $ch1)
                                       <li class="nav-cat-1">
                                           <a href="{{ url('/c/'.$ch1->url) }}">{{ $ch1->name }}</a>
                                           @if(count($ch1->children))
                                               <ul class="lvl2 parent-category-child-child">
                                                   @foreach($ch1->children as $ch2)
                                                       <li class="nav-cat-2">
                                                           <a href="{{ url('/c/'.$ch2->url) }}">{{ $ch2->name }}</a>
                                                       </li>
                                                   @endforeach
                                               </ul>
                                           @endif
                                       </li>
                                   @endforeach
                                   </ul>
                               @endif
                           </li>
                        @endforeach
                        </ul>
                        {{-- <div class="row">
                            @foreach(hlp()::categoryTree() as $cat)
                                <div class="col-3">
                                    <a href="{{ url('/c/'.$cat->url) }}">{{ $cat->name }}</a>
                                    @if(count($cat->children))
                                        @foreach($cat->children as $ch1)
                                            <div class="row">
                                                <div class="col-12">
                                                    - <a href="{{ url('/c/'.$ch1->url) }}">{{ $ch1->name }}</a>
                                                    @if(count($ch1->children))
                                                        @foreach($ch1->children as $ch2)
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    -- <a href="{{ url('/c/'.$ch2->url) }}">{{ $ch2->name }}</a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div> --}}
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/olla-info">Info</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Promotions</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
            </ul>
            <div class="d-flex text-white-50">
                <a class="nav-link text-white-50" href="/faq">FAQ's</a>
            </div>
        </div>
    </div>
</nav>

