<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{{ csrf_token() }}">

    @stack('before-styles')
    <link href="{{ asset('os_assets/vendor/bootstrap/5.1.3/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/theme/default.css') }}" rel="stylesheet">
    @stack('after-styles')

    <title>@yield('title')</title>
</head>
<body>

@include('os_views::frontend.layouts.partials.top-header')
@include('os_views::frontend.layouts.partials.navbar')

<main class="flex-shrink-0">
    <div class="container content">
        @yield('content')
    </div>
</main>

@include('os_views::frontend.layouts.partials.footer')

@stack('before-scripts')
<script src="{{ asset('os_assets/admin/vendor/jquery/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('os_assets/vendor/bootstrap/5.1.3/js/bootstrap.bundle.js') }}"></script>
@stack('after-scripts')

</body>
</html>


