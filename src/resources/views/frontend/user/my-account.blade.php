@extends('os_views::frontend.layouts.default')

@section('title', 'My Account')

@section('content')
    {{ pageTitle('My Account') }}
    <div class="row">
        <div class="col-md-3">
            <div class="col-12 btn-group-vertical" role="group">
                <button type="button" class="btn btn-outline-dark" data-option="account">Account</button>
                <button type="button" class="btn btn-outline-dark" data-option="change-password">Change Password</button>
                <button type="button" class="btn btn-outline-dark" data-option="addresses">Addresses</button>
                <button type="button" class="btn btn-outline-dark" data-option="orders">Orders</button>
                <button type="button" class="btn btn-outline-dark" data-option="favorites">Favorites</button>
            </div>
        </div>
        <div class="col-md-9" id="myAccountOption"></div>
    </div>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.cartQty', function (e) {
                updateCart($(this).attr('data-pid'), $(this).val())
            });
        });

    </script>
@endpush
