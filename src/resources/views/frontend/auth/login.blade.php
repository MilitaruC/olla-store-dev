@extends('os_views::frontend.layouts.default')

@section('title', 'Login')

@section('content')

    {{ pageTitle('Login') }}

    <form method="POST" action="{{ url('/login') }}">
        @csrf
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Name</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
        <button class="btn btn-outline-success" type="submit">Login</button>
    </form>

@endsection
