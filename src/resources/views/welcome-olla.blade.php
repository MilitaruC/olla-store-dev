@extends('ollastore::layouts.default')

@section('title', 'Olla Store Info Page')

@section('content')

<div>
    <h3>Customers</h3>
    <ul>
        @foreach(($customers ?? []) as $customer)
            <li>{{ $customer->name }}</li>
        @endforeach
    </ul>
</div>

@endsection
