<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{{ csrf_token() }}">

    @stack('before-styles')
    <link href="{{ asset('os_assets/vendor/bootstrap/5.1.3/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/theme/default.css') }}" rel="stylesheet">
    @stack('after-styles')

    <title>@yield('title')</title>
</head>
<body>

<main class="flex-shrink-0">
    <div class="container content">

    <h3>Login</h3>
    <form method="POST" action="{{ url('/admin/login') }}">
        @csrf

        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="name@example.com">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Name</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
        <button class="btn btn-outline-success" type="submit">Login</button>
    </form>
    </div>
</main>

@stack('before-scripts')
<script src="{{ asset('os_assets/admin/vendor/jquery/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('os_assets/vendor/bootstrap/5.1.3/js/bootstrap.bundle.js') }}"></script>
@stack('after-scripts')

</body>
</html>

