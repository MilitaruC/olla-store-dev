<nav class="sidebar">
    <header>
        <div class="logo-wrapper"></div>
        <a href="{{ url('/') }}" target="_blank" class="headerStoreName">Store Name</a>
    </header>
    <ul class="navbar-opened">
        <li><a href="{{ url('/admin') }}" class="@if(!request()->segment(2)) active @endif"><i class="fa-solid fa-table-columns"></i> Dashboard</a></li>
        <li><a href="{{ url('/admin/stores') }}" class="@if(request()->segment(2) == 'stores') active @endif"><i class="fa-solid fa-store"></i> {{  __('olla.Stores') }}</a></li>
        <li><a href="{{ url('/admin/admins') }}" class="@if(request()->segment(2) == 'admins') active @endif"><i class="fa-solid fa-user-shield"></i> {{  __('olla.Admins') }}</a></li>
        <li><a href="{{ url('/admin/customers') }}" class="@if(request()->segment(2) == 'customers') active @endif"><i class="fa-solid fa-users"></i> {{  __('olla.Customers') }}</a></li>
        <li><a href="{{ url('/admin/orders') }}" class="@if(request()->segment(2) == 'orders') active @endif"><i class="fa-solid fa-cart-arrow-down"></i> {{  __('olla.Orders') }}</a></li>
        <li><a href="{{ url('/admin/categories') }}" class="@if(request()->segment(2) == 'categories') active @endif"><i class="fa-solid fa-list"></i> {{  __('olla.Categories') }}</a></li>
        <li><a href="{{ url('/admin/products') }}" class="@if(request()->segment(2) == 'products') active @endif"><i class="fa-solid fa-cubes"></i> {{  __('olla.Products') }}</a></li>
        <li><a>Statistics</a></li>
        <li><a>Tickets</a></li>
        <li><a>FAQ</a></li>
        <li><a>Search</a></li>
        <li><a>Settings</a></li>
    </ul>
    <ul class="navbar-closed for-small-width">
        <li><a href="{{ url('/admin') }}" class="@if(!request()->segment(2)) active @endif"><i class="fa-solid fa-table-columns"></i></a></li>
        <li><a href="{{ url('/admin/stores') }}" class="@if(request()->segment(2) == 'stores') active @endif"><i class="fa-solid fa-store"></i></a></li>
        <li><a href="{{ url('/admin/admins') }}" class="@if(request()->segment(2) == 'admins') active @endif"><i class="fa-solid fa-user-shield"></i></a></li>
        <li><a href="{{ url('/admin/customers') }}" class="@if(request()->segment(2) == 'customers') active @endif"><i class="fa-solid fa-users"></i></a></li>
        <li><a href="{{ url('/admin/orders') }}" class="@if(request()->segment(2) == 'orders') active @endif"><i class="fa-solid fa-cart-arrow-down"></i></a></li>
        <li><a href="{{ url('/admin/categories') }}" class="@if(request()->segment(2) == 'categories') active @endif"><i class="fa-solid fa-list"></i></a></li>
        <li><a href="{{ url('/admin/products') }}" class="@if(request()->segment(2) == 'products') active @endif"><i class="fa-solid fa-cubes"></i></a></li>
    </ul>
</nav>
