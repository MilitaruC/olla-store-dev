<div class="d-flex justify-content-between top-nav">
    <h1>@yield('page-title')</h1>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            {{ strtoupper((Session::get('locale') ?? config('app.locale'))) }} <i class="fa-solid fa-flag"></i> {{ Session::get('locale') }}{{ __('olla.Change flag') }}
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            @foreach(config('translatable.locales') as $locale => $localeName)
                <li><a class="dropdown-item" href="{{ url('/admin/language/' . $localeName) }}">{{ $localeName }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="fa-solid fa-user-shield"></i> {{ (auth()->user()->name ?? '') }}
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="{{ url('/admin/admins/my-account') }}">My account</a></li>
            <li><a class="dropdown-item" href="{{ url('/admin/logout') }}">Logout</a></li>
        </ul>
    </div>
</div>
