<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{{ csrf_token() }}">

    @stack('before-styles')
    <link href="{{ asset('os_assets/admin/vendor/bootstrap/5.1.3/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/admin//vendor/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('os_assets/admin/theme/default.css') }}" rel="stylesheet">
    @stack('after-styles')

    <title>@yield('title')</title>
</head>
<body>
    <div class="wrapper bg-dark">
        @include('os_views::admin.layouts.partials.sidebar')
        <main>
            @include('os_views::admin.layouts.partials.top-nav')
            @yield('content')
        </main>
    </div>
    <div class="bottom-footer"><h5>Flexbox Admin Template</h5></div>

    @stack('before-scripts')
    <script src="{{ asset('os_assets/admin/vendor/jquery/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('os_assets/admin/vendor/bootstrap/5.1.3/js/bootstrap.bundle.js') }}"></script>
    @stack('after-scripts')
</body>
</html>



