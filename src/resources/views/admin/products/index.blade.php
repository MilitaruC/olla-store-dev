@extends('os_views::admin.layouts.default')
@section('title', 'Products')
@section('page-title', 'Products')
@section('content')
    <div class="d-flex flex-row modelControls">
        <a href="{{ url('/admin/products/create') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-plus"></i> Add</a>
    </div>
    <div class="row card-white">
        <div class="col-md-12">
            <br>
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 100%">
                    {!! \Session::get('success') !!}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="d-flex flex-row searchInputs">
                <select name="perPage" id="perPage" class="form-control border-light-blue me-2">
                    <option value="5">5</option>
                    <option value="10" selected>10</option>
                    <option value="15">15</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <select name="sortBy" id="sortBy" class="form-control border-light-blue me-2">
                    <option value="products.id">ID</option>
                    <option value="translations.name">Name</option>
                </select>
                <select name="sortOrder" id="sortOrder" class="form-control border-light-blue me-2">
                    <option value="DESC">DESC</option>
                    <option value="ASC">ASC</option>
                </select>
                <select name="activeStatus" id="activeStatus" class="form-control border-light-blue me-2">
                    <option value="">Active/Inactive</option>
                    <option value="true">Active</option>
                    <option value="false">Inactive</option>
                </select>
                <input type="text" name="search" id="search" class="form-control border-light-blue" placeholder="Search">
            </div>
            <div class="d-flex flex-row" id="productsAjaxList"></div>
        </div>

    </div>
@endsection
@push('after-styles')
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
@endpush
@push('after-scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>

$(document).ready(function() {

    let model = {
        name: 'product',
        names: 'products',
        Name: 'Product',
        Names: 'Products',
        urlSearch: '/admin/products/ajax-list',
        urlStore: '/admin/products/store',
        urlUpdate: '/admin/products/update/',
        urlUpdateStatus: '/admin/products/update/active-status/',
        urlDestroy: '/admin/products/destroy/',
        urlLoadVendorStores: '/admin/products/load-vendor-stores',
        urlLoadVendorStoresCategories: '/admin/products/load-vendor-stores-categories',
        sendData: {
            perPage: 10,
            page: '1',
            sortBy: 'id',
            sortOrder: 'DESC',
            search: '',
            activeStatus: null,
            storeID: null,
        }
    };

    filterModel();

    $(document).on('click', '.pagination a', function(e) {
        e.preventDefault();

        let url = new URL($(this).attr('href'));
        let page = url.searchParams.get('page');

        model.sendData.page = page;

        // var url = $(this).attr('href');
        filterModel();
        // window.history.pushState("", "", url);
    });
    $(document).on('keyup', '#search', function (e) {
        model.sendData.page = '1';
        clearTimeout(timeout);
        timeout = setTimeout(function () { filterModel(); }, 500);
    });
    $(document).on('change', '#perPage', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#sortBy', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#sortOrder', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#activeStatus', function (e) {
        model.sendData.page = '1';
        filterModel();
    });

    var timeout = null;

    function filterModel(page = null)
    {
        if(page) model.sendData.page = page;
        model.sendData.search = $('#search').val();
        model.sendData.perPage = $('#perPage').val();
        model.sendData.sortBy = $('#sortBy').val();
        model.sendData.sortOrder = $('#sortOrder').val();
        model.sendData.activeStatus = $('#activeStatus').val();
        model.sendData.storeID = $('#storeID').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: model.urlSearch,
            method: 'post',
            data: model.sendData,
            success: function (data) {
                $('#'+model.names+'AjaxList').html(data);
            },
            error: function (data) {
                alert('Error!');
            }
        });
    }
});
</script>
@endpush
