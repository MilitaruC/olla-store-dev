<div class="d-flex flex-column flex-fill bd-highlight mb-3">
    <div class="d-flex justify-content-between pagination-wrapper">
        @if(count($products))
            {{ $products->links()}}
            <div class="modelTotals">{{ $products->count()}} / {{ $products->total()}} </div>
        @endif
    </div>
    <table class="table table-striped table-hover">
        <thead class="table-header-light-blue">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Sku</th>
            <th scope="col">Image</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Active</th>
            <th scope="col">Categories</th>
            <th scope="col">Updated</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <th scope="row" style="width:30px;">{{ $product->id }}</th>
            <td>{{ $product->sku }}</td>
            <td>{{ ($product->image ?? '') }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ round($product->price, 2) }}</td>
            <td>@if($product->active == 1) <i class="fa-solid fa-square-check" style="color: #2473bf"></i> @else <i class="fa-solid fa-square" style="color: #535353"></i> @endif</td>
            <td>{{ implode(', ', $product->categories->pluck('name')->toArray()) }}</td>
            <td>{{ $product->updated_at }}</td>
            <td style="width: 100px"><a href="{{ url('/admin/products/edit/'.$product->id) }}" class="btn btn-sm btn-success"><i class="fa-solid fa-square-pen"></i></a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="panel-controls pull-right">
        @if(count($products)) {{ $products->links()}} @endif
    </div>
</div>
