<table class="table table-striped table-hover">
    <thead class="table-header-light-blue">
    <tr>
        <th scope="col" style="width: 160px;">Sku</th>
        <th scope="col">Name</th>
        <th scope="col" style="width: 100px;">Price</th>
        <th scope="col" style="width: 120px;">Stock</th>
        <th scope="col">Active</th>
        @if(count($productAttributes))
            @foreach($productAttributes as $productAttribute)
            <th scope="col">{{ $productAttribute->name }}</th>
            @endforeach
        @endif
        <th scope="col" style="width: 200px">Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="5"><button type="button" class="btn btn-sm btn-primary addNewVariant"><i class="fa-solid fa-plus"></i> Add New Variant</button></td>
        @if(count($productAttributes))
            @foreach($productAttributes as $productAttribute)
                <td></td>
            @endforeach
        @endif
        <td></td>
    </tr>
    @foreach($products as $product)
        <tr class="product_variant" data-product-id="{{ $product->id }}" id="variant_{{ $product->id }}" @if(($currentProduct->id ?? null) == $product->id) style="background: #2473bf;" @endif>
            <td><input type="text" id="variant_sku_{{ $product->id }}" class="form-control form-control-sm border-light-blue pv variant_sku" value="{{ $product->sku }}" data-product-id="{{ $product->id }}"></td>
            <td><input type="text" id="variant_name_{{ $product->id }}" class="form-control form-control-sm border-light-blue pv variant_name" value="{{ $product->name }}" data-product-id="{{ $product->id }}"></td>
            <td><input type="text" id="variant_price_{{ $product->id }}" class="form-control form-control-sm border-light-blue pv" value="{{ $product->price }}" data-product-id="{{ $product->id }}"></td>
            <td><input type="number" id="variant_stock_{{ $product->id }}" class="form-control form-control-sm border-light-blue pv" value="{{ $product->stock }}" data-product-id="{{ $product->id }}"></td>
            <td>@if($product->active == 1) Active @else No @endif</td>
            @if(count($productAttributes))
                @foreach($productAttributes as $attribute)
                    <td>
                        <select name="product_attribute_value_{{ $product->id }}" id="product_{{ $product->id }}_attribute_{{ $attribute->id }}"
                                class="form-control form-control-sm border-light-blue variant_attribute_value pv variant_{{ $product->id }}"
                                data-product-id="{{ $product->id }}" data-attribute-id="{{ $attribute->id }}">
                            <option value="">Select an option</option>
                            @foreach($attribute->attributeValues as $attributeValue)
                                <option value="{{ $attributeValue->id }}"
                                    @if(in_array($attributeValue->id, ($product->selectedAttributeValues ?? []))) selected @endif
                                >{{ $attributeValue->value }}</option>
                            @endforeach
                        </select>
                    </td>
                @endforeach
            @endif
            <td>
{{--                <button class="btn btn-sm btn-success">Edit</button>--}}
                @if($product->unsaved ?? null)
                    <button class="btn btn-sm btn-danger" onclick="$('#variant_{{ $product->id }}').remove()">Remove</button>
                @else
                    @if($product->active == 1)
                        <button class="btn btn-sm btn-warning" id="dpv-{{ $product->id }}" class="deactivateVariant" data-product-id="{{ $product->id }}">Deactivate</button>
                    @else
                        <button class="btn btn-sm btn-warning" id="apv-{{ $product->id }}" class="activateVariant" data-product-id="{{ $product->id }}">Activate</button>
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
