@if(count($product->attributes ?? []) || ($productAttributes[0] ?? null))
<table class="table table-striped table-hover">
    <thead class="table-header-light-blue">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Value</th>
        <th scope="col">Active</th>
        <th scope="col">Display on Store</th>
        <th scope="col" style="width: 200px">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($productAttributes as $attribute)
        <tr>
            <td>{{ $attribute->name }}</td>
            <td>
                <select name="product_attribute_values[{{ $attribute->id }}]" id="product_attribute_{{ $attribute->id }}" class="form-control border-light-blue product_attribute_value" data-attribute-id="{{ $attribute->id }}">
                    <option value="">Select an option</option>
                    @foreach($attribute->attributeValues as $attributeValue)
                        <option value="{{ $attributeValue->id }}"
                            @if(in_array($attributeValue->id, ($requestProductAttributeValues ?? [])) || in_array($attributeValue->id, $selectedProductAttributeValues)) selected @endif
                        >{{ $attributeValue->value }}</option>
                    @endforeach
                </select>
            </td>
            <td>Active</td>
            <td>Yes</td>
            <td>
                <button class="btn btn-sm btn-success"><i class="fa-solid fa-square-pen"></i> Edit</button>
                <button class="btn btn-sm btn-danger"><i class="fa-solid fa-trash"></i> Delete</button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endif
