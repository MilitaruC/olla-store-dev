@extends('os_views::admin.layouts.default')
@section('title', 'Add new product')
@section('page-title', 'Add new product')
@section('content')
    <div class="d-flex flex-row modelControls">
        <a href="{{ url('/admin/products') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-chevron-left"></i> Back</a>
    </div>

    <div class="row card-white">
        <div class="col-md-12 p-4 modelSection"><h3>Info</h3></div>
        @if ($errors->any())
            <div class="col-md-12 p-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        @if (\Session::has('fail'))
            <div class="col-md-12 p-4">
                <div class="alert alert-warning" style="width: 100%">
                    <ul>
                        <li>{!! \Session::get('fail') !!}</li>
                    </ul>
                </div>
            </div>
        @endif
        <form action="{{ url('/admin/products/store') }}" method="post" enctype="multipart/form-data">
            <div class="col-md-12 p-4">
                @csrf
                <div class="mb-3 form-check form-check-inline">
                    <input type="checkbox" name="active" class="form-check-input border-light-blue" id="active">
                    <label class="form-check-label" for="active">Active</label>
                </div>
                <div class="mb-3">
                    <label for="parent_id" class="form-label selectParent">
                        <i class="fa-solid fa-angle-down"></i>
                        <i class="fa-solid fa-angle-up hidden"></i> Categories
                    </label>
                    <input type="text" class="form-control border-light-blue hidden" name="categories" id="categories" value="{{ (old('categories') ?? '') }}">
                    <div id="categoriesTree" class="hidden"></div>
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control border-light-blue" name="name" id="name" aria-describedby="nameHelp" value="{{ old('name') }}">
                </div>
                <div class="mb-3">
                    <label for="sku" class="form-label">SKU</label>
                    <input type="text" class="form-control border-light-blue" name="sku" id="sku" aria-describedby="skuHelp" value="{{ old('sku') }}">
                </div>
                <div class="row">
                    <div class="mb-3 col-md-6">
                        <label for="type_id" class="form-label">Product type</label>
                        <select name="type_id" id="type_id" class="form-control border-light-blue">
                            @foreach($productTypes as $productType)
                                <option value="{{ $productType->id }}">{{ $productType->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="configurable" class="form-label">Configurable</label>
                        <select name="configurable" id="configurable" class="form-control border-light-blue">
                            <option value="0" selected>No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input type="text" class="form-control border-light-blue" name="price" id="price" aria-describedby="priceHelp" value="{{ old('price') }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="vat_price" class="form-label">Price VAT</label>
                        <input type="text" class="form-control border-light-blue" name="vat_price" id="vat_price" aria-describedby="vat_priceHelp" value="{{ old('vat_price') }}" disabled>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="old_price" class="form-label">Old Price</label>
                        <input type="text" class="form-control border-light-blue" name="old_price" id="old_price" aria-describedby="old_priceHelp" value="{{ old('old_price') }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="old_vat" class="form-label">Old Price VAT</label>
                        <input type="text" class="form-control border-light-blue" name="old_vat" id="old_vat" aria-describedby="old_vatHelp" value="{{ old('old_vat') }}" disabled>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="short_description" class="form-label">Short description</label>
                    <textarea class="form-control border-light-blue" name="short_description" id="short_description" rows="10">{{ old('short_description') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea class="form-control border-light-blue" name="description" id="description" rows="3">{{ old('description') }}</textarea>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productSeo"><h3>SEO</h3></div>
            <div class="col-md-12 p-4 productSeo">
                <div class="mb-3">
                    <label for="url" class="form-label">Url</label>
                    <input type="text" class="form-control border-light-blue" name="url" id="url" aria-describedby="urlHelp" value="{{ old('url') }}">
                </div>
                <div class="mb-3">
                    <label for="meta_title" class="form-label">Meta title</label>
                    <input type="text" class="form-control border-light-blue" name="meta_title" id="meta_title" aria-describedby="meta_titleHelp" value="{{ old('meta_title') }}">
                </div>
                <div class="mb-3">
                    <label for="meta_key" class="form-label">Meta keys</label>
                    <textarea class="form-control border-light-blue" name="meta_key" id="meta_key" rows="1">{{ old('meta_key') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="meta_desc" class="form-label">Meta description</label>
                    <textarea class="form-control border-light-blue" name="meta_desc" id="meta_desc" rows="2">{{ old('meta_desc') }}</textarea>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productStock"><h3>Stock</h3></div>
            <div class="col-md-12 p-4 productSrock">
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="stock" class="form-label">Stock</label>
                        <input type="number" class="form-control border-light-blue" name="stock" id="stock" aria-describedby="stockHelp" value="{{ old('stock') }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="min_quantity" class="form-label">Min Quantity</label>
                        <input type="number" class="form-control border-light-blue" name="min_quantity" id="min_quantity" aria-describedby="min_quantityHelp" value="{{ old('min_quantity') ?? 1 }}" disabled>
                    </div>
                </div>

            </div>

            <div class="col-md-12 p-4 modelSection" data-section="categorySeo"><h3>Gallery</h3></div>
            <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
            <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
            <div class="col-md-12 p-4 productgallery">
                <div class="mb-3">
                    <label for="url" class="form-label">Images</label>
                    <input type="file" name="file" id="file" />
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productAttributes"><h3>Attributes</h3></div>
            <div class="col-md-12 p-4 productAttributes">
                <input type="hidden" name="product_attribute_values" id="product_attribute_values" value="{{ old('product_attribute_values') }}">
                <div class="row">
                    <div class="mb-3">
                        <label for="product_attributes" class="form-label">Product attributes</label>
                        <select name="product_attributes[]" id="product_attributes" class="form-control border-light-blue select2" multiple="multiple">
                            @foreach($attributes as $productAttribute)
                                <option value="{{ $productAttribute->id }}">{{ $productAttribute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3" id="productAttributes">

                    </div>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productVariants"><h3>Variants</h3></div>
            <div class="col-md-12 p-4 productVariants">
                <div class="row">

                </div>
            </div>

            <button type="submit" class="btn btn-primary hidden" id="submitCreateProductForm">Submit</button>
        </form>
    </div>
    <button type="button" class="btn btn-primary saveModel" onclick="$('#submitCreateProductForm').trigger('click');">Save</button>

@endsection
@push('after-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>
    <style>
        .select2-selection{
            border-radius: 0px !important;
            border-color: #2473bf !important;
            padding: 0.375rem 0.75rem !important;
        }
    </style>
@endpush
@push('after-scripts')
    <script src="{{ asset('os_assets/admin/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        var categories = [];
        var excludeCategories = [0,'0','#'];
        var jsondata = {!! $json !!} ;
        var productAttributes = [];
        var selectedAttributeValues = [];

        $(document).ready(function () {

            $('.select2').select2();
            createJSTree(jsondata);
            loadProductAttributes();

            $('body').on('click', '.selectParent', function (e){
                $('#categoriesTree').toggleClass('hidden');
                $('.selectParent .fa-angle-down').toggleClass('hidden');
                $('.selectParent .fa-angle-up').toggleClass('hidden');
            });

            $("#categoriesTree").on("changed.jstree", function (e, data) {
                    // e.stopPropagation();
                    // e.preventDefault();
                    if(data.node){
                        // console.log('selectedJstreeCheckboxChildren : ' + selectedJstreeCheckboxChildren);
                        if(data.node.state.selected === true){
                            if (!excludeCategories.includes(data.node.id)) { jsondata[data.node.id].state.selected = true; }
                            console.log('SELECT: ', data.node);
                            if(data.node.parents.length > 0){
                                data.node.parents.forEach(id => {
                                    console.log('id select: ', id);
                                    if(id !== '#'){
                                        $('#'+id+'_anchor').addClass('jstree-clicked');
                                        $('#'+id).attr('aria-selected', 'true');
                                    }
                                    if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = true; }

                                });
                            }
                            if(data.node.children_d.length > 0){
                                data.node.children_d.forEach(id => {
                                    console.log('id remove CH: ', id);
                                    if(id !== '#'){
                                        $('#'+id+'_anchor').removeClass('jstree-clicked');
                                        $('#'+id).attr('aria-selected', 'false');
                                    }
                                    if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = false; }
                                });
                            }
                        }
                        if(data.node.state.selected === false){
                            if (!excludeCategories.includes(data.node.id)) { jsondata[data.node.id].state.selected = false; }
                            console.log('NOT SELECT: ', data.node);
                            if(data.node.parents.length > 0){
                                data.node.parents.forEach(id => {
                                    console.log('id select: ', id);
                                    if(id !== '#'){
                                        $('#'+id+'_anchor').addClass('jstree-clicked');
                                        $('#'+id).attr('aria-selected', 'true');
                                    }
                                    if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = true; }
                                });
                            }
                            if(data.node.children_d.length > 0){
                                data.node.children_d.forEach(id => {
                                    console.log('id remove CH: ', id);
                                    if(id !== '#'){
                                        $('#'+id+'_anchor').removeClass('jstree-clicked');
                                        $('#'+id).attr('aria-selected', 'false');
                                    }
                                    if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = false; }
                                });
                            }
                        }
                        console.log(data.node);
                        // $('#categoriesTree').jstree(jsondata).refresh();
                        createJSTree(jsondata);
                    }
                })
                .jstree({
                    "plugins" : [ "changed" ]
                });

            $("body").on("change", '#product_attributes', function (e) {
                selectedAttributeValues = [];
                productAttributes = $('#product_attributes').val();
                $('.product_attribute_value').each(function(i, obj) {
                    if($(obj).val()){
                        if(productAttributes.includes($(obj).attr('data-attribute-id'))){ selectedAttributeValues[$(obj).attr('data-attribute-id')] = $(obj).val();}
                    }
                });
                selectedAttributeValues = selectedAttributeValues.filter(function (el) { return el != null; });
                $('#product_attribute_values').val(selectedAttributeValues);
                loadProductAttributes();
            });

            $("body").on("change", '.product_attribute_value', function (e) {
                selectedAttributeValues = [];
                productAttributes = $('#product_attributes').val();
                $('.product_attribute_value').each(function(i, obj) {
                    if($(obj).val()){
                        if(productAttributes.includes($(obj).attr('data-attribute-id'))){ selectedAttributeValues[$(obj).attr('data-attribute-id')] = $(obj).val();}
                    }
                });
                selectedAttributeValues = selectedAttributeValues.filter(function (el) { return el != null; });
                $('#product_attribute_values').val(selectedAttributeValues);
            });

            var ckeditorToolbar = {
                // Define the toolbar groups as it is a more accessible solution.
                toolbarGroups: [
                    { "name": "basicstyles", "groups": ["basicstyles"] },
                    { "name": "links", "groups": ["links"] },
                    { "name": "paragraph", "groups": ["list", "blocks"] },
                    { "name": "document", "groups": ["mode"] },
                    { "name": "insert", "groups": ["insert"] },
                    { "name": "styles", "groups": ["styles"] },
                    { "name": "about", "groups": ["about"] }
                ],
                // Remove the redundant buttons from toolbar groups defined above.
                removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
            };

            CKEDITOR.replace('short_description', ckeditorToolbar);
            CKEDITOR.replace('description', ckeditorToolbar);
        });

        function createJSTree(jsondata) {
            var sortType = "asc";
            $('#categoriesTree').on('ready.jstree', (e, data) => {
                data.instance.sort = () => {};
            });
            $('#categoriesTree').jstree({
                "core": { "check_callback" : true, 'data': jsondata },
                "checkbox" : {
                    "keep_selected_style" : false,
                    three_state: false,
                    // cascade: 'up'
                },
                "plugins" : [ "search", "sort", "types", "checkbox" ],
                sort : function(a, b) {
                    a1 = this.get_node(a);
                    b1 = this.get_node(b);
                    if (sortType === "asc"){ return (a1.data.sort > b1.data.sort) ? 1 : -1; }
                    else { return (a1.data.sort > b1.data.sort) ? -1 : 1; }
                }
            });
            console.log('CREATED', jsondata);
            categories = [];
            jsondata.forEach(el => {
                if(el.state.selected === true && !excludeCategories.includes(el.id)){
                    var id = el.id;
                    // categories.push({category_id: el.id});
                    if(id) categories[id] = id;
                }
            });
            $('#categories').val(JSON.stringify(categories));
            console.log('Categories', categories, JSON.stringify(categories));
        }

        function loadProductAttributes(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/products/loadProductAttributes',
                method: 'get',
                data: {
                    productAttributes: productAttributes,
                    productAttributeValues: selectedAttributeValues,
                },
                success: function (data) {
                    $('#productAttributes').html(data.productAttributes);
                    // console.log(data.request)
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }
    </script>
@endpush
