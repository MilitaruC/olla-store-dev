@extends('os_views::admin.layouts.default')
@section('title', 'Edit product "' . $product->name . '"')
@section('page-title', 'Edit product "' . $product->name . '"')
@section('content')
    <div class="d-flex flex-row modelControls">
        <a href="{{ url('/admin/products') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-chevron-left"></i> Back</a>
        <a href="{{ url('/admin/products/create') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-plus"></i> Add</a>
    </div>

    <div class="row card-white">
        <div class="col-md-12 p-4 modelSection"><h3>Info</h3></div>
        @if ($errors->any())
            <div class="col-md-12 p-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        @if (\Session::has('fail'))
            <div class="col-md-12 p-4">
                <div class="alert alert-warning" style="width: 100%">
                    <ul>
                        <li>{!! \Session::get('fail') !!}</li>
                    </ul>
                </div>
            </div>
        @endif
        <form action="{{ url('/admin/products/update/'.$product->id) }}" method="post" enctype="multipart/form-data">
            <div class="col-md-12 p-4">
                @csrf
                <div class="mb-3 form-check form-check-inline">
                    <input type="checkbox" name="active" class="form-check-input border-light-blue" id="active" @if($product->active == 1) checked="checked" @endif >
                    <label class="form-check-label" for="active">Active</label>
                </div>
                <div class="mb-3">
                    <label for="parent_id" class="form-label selectParent">
                        <i class="fa-solid fa-angle-down"></i>
                        <i class="fa-solid fa-angle-up hidden"></i> Categories
                    </label>
                    <input type="text" class="form-control border-light-blue hidden" name="categories" id="categories" value="">
                    <div id="categoriesTree" class="hidden"></div>
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control border-light-blue" name="name" id="name" aria-describedby="nameHelp" value="{{ old('name') ?? $product->name }}">
                </div>
                <div class="mb-3">
                    <label for="sku" class="form-label">SKU</label>
                    <input type="text" class="form-control border-light-blue" name="sku" id="sku" aria-describedby="skuHelp" value="{{ old('sku') ?? $product->sku }}">
                </div>
                <div class="row">
                    <div class="mb-3 col-md-6">
                        <label for="type_id" class="form-label">Product type</label>
                        <select name="type_id" id="type_id" class="form-control border-light-blue">
                            @foreach($productTypes as $productType)
                                <option value="{{ $productType->id }}" @if($productType->id == $product->type_id) selected @endif>{{ $productType->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3 col-md-6">
                        <label for="configurable" class="form-label">Configurable</label>
                        <select name="configurable" id="configurable" class="form-control border-light-blue">
                            <option value="0" @if($product->configurable == 0) selected @endif>No</option>
                            <option value="1" @if($product->configurable == 1) selected @endif>Yes</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12 p-4 modelSection productVariants @if($product->configurable < 1) hidden @endif" data-section="productVariants"><h3>Variants</h3></div>
                <div class="col-md-12 p-4 productVariants @if($product->configurable < 1) hidden @endif">
                    <div class="mb-3">
                        <label for="variant_attributes" class="form-label">Product attributes</label>
                        <select name="variant_attributes[]" id="variant_attributes" class="form-control border-light-blue select2" multiple="multiple">
                            @foreach($attributes as $productAttribute)
                                <option value="{{ $productAttribute->id }}" @if(in_array($productAttribute->id, $product->attributes->pluck('id')->toArray())) selected @endif>{{ $productAttribute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="productVariants"></div>
                    <br><br><br>
                </div>

                <div class="row productPrice">
                    <div class="col-md-3 mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input type="text" class="form-control border-light-blue" name="price" id="price" aria-describedby="priceHelp" value="{{ old('price') ?? $product->price }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="vat_price" class="form-label">Price VAT</label>
                        <input type="text" class="form-control border-light-blue" name="vat_price" id="vat_price" aria-describedby="vat_priceHelp" value="{{ old('vat_price') ?? $product->vat_price }}" disabled>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="old_price" class="form-label">Old Price</label>
                        <input type="text" class="form-control border-light-blue" name="old_price" id="old_price" aria-describedby="old_priceHelp" value="{{ old('old_price') ?? $product->old_price }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="old_vat" class="form-label">Old Price VAT</label>
                        <input type="text" class="form-control border-light-blue" name="old_vat" id="old_vat" aria-describedby="old_vatHelp" value="{{ old('old_vat') ?? $product->old_vat }}" disabled>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="short_description" class="form-label">Short description</label>
                    <textarea class="form-control border-light-blue" name="short_description" id="short_description" rows="10">{{ old('short_description') ?? $product->short_description }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea class="form-control border-light-blue" name="description" id="description" rows="3">{{ old('description') ?? $product->description }}</textarea>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productSeo"><h3>SEO</h3></div>
            <div class="col-md-12 p-4 productSeo">
                <div class="mb-3">
                    <label for="url" class="form-label">Url</label>
                    <input type="text" class="form-control border-light-blue" name="url" id="url" aria-describedby="urlHelp" value="{{ old('url') ?? $product->url }}">
                </div>
                <div class="mb-3">
                    <label for="meta_title" class="form-label">Meta title</label>
                    <input type="text" class="form-control border-light-blue" name="meta_title" id="meta_title" aria-describedby="meta_titleHelp" value="{{ old('meta_title') ?? $product->meta_title }}">
                </div>
                <div class="mb-3">
                    <label for="meta_key" class="form-label">Meta keys</label>
                    <textarea class="form-control border-light-blue" name="meta_key" id="meta_key" rows="1">{{ old('meta_key') ?? $product->meta_key }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="meta_desc" class="form-label">Meta description</label>
                    <textarea class="form-control border-light-blue" name="meta_desc" id="meta_desc" rows="2">{{ old('meta_desc') ?? $product->meta_desc }}</textarea>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="productStock"><h3>Stock</h3></div>
            <div class="col-md-12 p-4 productStock">
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="stock" class="form-label">Stock</label>
                        <input type="number" class="form-control border-light-blue" name="stock" id="stock" aria-describedby="stockHelp" value="{{ old('stock') ?? $product->stock }}">
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="min_quantity" class="form-label">Min Quantity</label>
                        <input type="number" class="form-control border-light-blue" name="min_quantity" id="min_quantity" aria-describedby="min_quantityHelp" value="{{ old('min_quantity') ?? $product->min_quantity ?? 1 }}" disabled>
                    </div>
                </div>

            </div>

            <div class="col-md-12 p-4 modelSection" data-section="categorySeo"><h3>Gallery</h3></div>

            <div class="col-md-12 p-4 productgallery">
                <div class="mb-3">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalUploadToProductGallery">Upload More to Gallery</button>
                </div>
                @if(count($product->parentImages))
                    <div class="mb-3">
                        @foreach($product->parentImages as $image)
                            <img src="{{ url($image->rel_path . $image->file) }}" style="height: 200px">
                        @endforeach
                    </div>
                @endif
            </div>

            <div class="col-md-12 p-4 modelSection productAttributes @if($product->configurable > 0) hidden @endif" data-section="productAttributes"><h3>Attributes</h3></div>
            <div class="col-md-12 p-4 productAttributes @if($product->configurable > 0) hidden @endif">
                <input type="hidden" name="product_attribute_values" id="product_attribute_values" value="{{ json_encode($product->attributeValues->pluck('id')->toArray()) }}">
                <div class="row">
                    <div class="mb-3">
                        <label for="product_attributes" class="form-label">Product attributes</label>
                        <select name="product_attributes[]" id="product_attributes" class="form-control border-light-blue select2" multiple="multiple">
                            @foreach($attributes as $productAttribute)
                                <option value="{{ $productAttribute->id }}" @if(in_array($productAttribute->id, $product->attributes->pluck('id')->toArray())) selected @endif>{{ $productAttribute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3" id="productAttributes">

                    </div>
                </div>
            </div>

            <input type="hidden" name="variants" id="variants" value="">

            <button type="submit" class="btn btn-primary hidden" id="submitCreateProductForm">Submit</button>


        </form>

        <br><br><br><br><br><br>
    </div>
    <button type="button" class="btn btn-primary saveModel" onclick="setFormData()">Save</button>

    <div id="dialog" title="Warning"></div>



    <!-- Modal -->
    <div class="modal fade" id="modalUploadToProductGallery" tabindex="-1" aria-labelledby="modalUploadToProductGalleryLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUploadToProductGalleryLabel">Product Gallery Upload</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Fine Uploader DOM Element ====================================================================== -->
                    <div id="product-gallery-upload"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
{{--                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>

    <br><br><br><br><br>

@endsection
@push('after-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link href="{{ asset('/os_assets/admin/vendor/fine-uploader/fine-uploader-new.css') }}" rel="stylesheet">
    <style>
        .select2-selection{
            border-radius: 0px !important;
            border-color: #2473bf !important;
            padding: 0.375rem 0.75rem !important;
        }
        .hasError{
            border-color: red;
        }
    </style>
@endpush
@push('after-scripts')
    <script src="{{ asset('os_assets/admin/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="{{ asset('/os_assets/admin/vendor/fine-uploader/fine-uploader.js') }}"></script>

<script type="text/template" id="qq-template-manual-trigger">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="buttons">
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Select files</div>
            </div>
            <button type="button" id="trigger-upload" class="btn btn-primary">
                <i class="icon-upload icon-white"></i> Upload
            </button>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">No</button>
                <button type="button" class="qq-ok-button-selector">Yes</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>

<style>
    #trigger-upload {
        color: white;
        background-color: #00ABC7;
        font-size: 14px;
        padding: 7px 20px;
        background-image: none;
    }
    #product-gallery-upload{
        margin-bottom: 40px;
    }
    #product-gallery-upload .qq-upload-button {
        margin-right: 15px;
    }

    #product-gallery-upload .buttons {
        width: 36%;
    }

    #product-gallery-upload .qq-uploader .qq-total-progress-bar-container {
        width: 60%;
    }
</style>

<!-- Your code to create an instance of Fine Uploader and bind to the DOM/template
====================================================================== -->
<script>
    // https://github.com/FineUploader/fine-uploader
    // https://fineuploader.com/demos
    var manualUploader = new qq.FineUploader({
        element: document.getElementById('product-gallery-upload'),
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/admin/images/ajax-store-to-product/{{ $product->id }}',
            customHeaders: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/os_assets/admin/vendor/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/os_assets/admin/vendor/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        autoUpload: false,
        debug: true,
        callbacks: {
            onAllComplete: function() {
                alert('done')
            }
        }
    });

    qq(document.getElementById("trigger-upload")).attach("click", function() {
        manualUploader.uploadStoredFiles();
    });
</script>
    <script>
        var jsondata = {!! $json !!} ;
        var categories = [];
        var excludeCategories = [0,'0','#'];
        var productAttributes = [];
        var selectedAttributeValues = [];
        var variants = [];
        var variant = {};

        $(document).ready(function () {

            $('.select2').select2();
            createJSTree(jsondata);
            if({{ $product->configurable }} == 1){ loadProductVariants(); } else { loadProductAttributes(); }

            $('body').on('click', '.selectParent', function (e){
                $('#categoriesTree').toggleClass('hidden');
                $('.selectParent .fa-angle-down').toggleClass('hidden');
                $('.selectParent .fa-angle-up').toggleClass('hidden');
            });

            $("#categoriesTree").on("changed.jstree", function (e, data) {
                if(data.node){
                    if(data.node.state.selected === true){
                        if (!excludeCategories.includes(data.node.id)) { jsondata[data.node.id].state.selected = true; }
                        console.log('SELECT: ', data.node);
                        if(data.node.parents.length > 0){
                            data.node.parents.forEach(id => {
                                console.log('id select: ', id);
                                if(id !== '#'){
                                    $('#'+id+'_anchor').addClass('jstree-clicked');
                                    $('#'+id).attr('aria-selected', 'true');
                                }
                                if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = true; }

                            });
                        }
                        if(data.node.children_d.length > 0){
                            data.node.children_d.forEach(id => {
                                console.log('id remove CH: ', id);
                                if(id !== '#'){
                                    $('#'+id+'_anchor').removeClass('jstree-clicked');
                                    $('#'+id).attr('aria-selected', 'false');
                                }
                                if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = false; }
                            });
                        }
                    }
                    if(data.node.state.selected === false){
                        if (!excludeCategories.includes(data.node.id)) { jsondata[data.node.id].state.selected = false; }
                        console.log('NOT SELECT: ', data.node);
                        if(data.node.parents.length > 0){
                            data.node.parents.forEach(id => {
                                console.log('id select: ', id);
                                if(id !== '#'){
                                    $('#'+id+'_anchor').addClass('jstree-clicked');
                                    $('#'+id).attr('aria-selected', 'true');
                                }
                                if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = true; }
                            });
                        }
                        if(data.node.children_d.length > 0){
                            data.node.children_d.forEach(id => {
                                console.log('id remove CH: ', id);
                                if(id !== '#'){
                                    $('#'+id+'_anchor').removeClass('jstree-clicked');
                                    $('#'+id).attr('aria-selected', 'false');
                                }
                                if (!excludeCategories.includes(id)) { jsondata[id++].state.selected = false; }
                            });
                        }
                    }
                    console.log(data.node);
                    // $('#categoriesTree').jstree(jsondata).refresh();
                    createJSTree(jsondata);
                }
            }).jstree({
                "plugins" : [ "changed" ]
            });

            $("body").on("change", '#configurable', function (e) {
                console.log($('#configurable').val());
                if($('#configurable').val() == 1){
                    $('.productVariants').removeClass('hidden');
                    $('.productAttributes').addClass('hidden');
                    $('.productPrice').addClass('hidden');
                    $('.select2').select2();
                    loadProductVariants();
                } else {
                    $('.productVariants').addClass('hidden');
                    $('.productAttributes').removeClass('hidden');
                    $('.productPrice').removeClass('hidden');
                    $('.select2').select2();
                    loadProductAttributes();
                }
            });

            $("body").on("click", '.addNewVariant', function (e) {
                loadProductVariants(1);
            });

            $("body").on("change", '#variant_attributes', function (e) {
                loadProductVariants();
            });

            $("body").on("change", '.variant_attribute_value', function (e) {
                loadProductVariants();
            });

            $("body").on("change", '#product_attributes', function (e) {
                selectedAttributeValues = [];
                productAttributes = $('#product_attributes').val();
                $('.product_attribute_value').each(function(i, obj) {
                    if($(obj).val()){
                        if(productAttributes.includes($(obj).attr('data-attribute-id'))){ selectedAttributeValues[$(obj).attr('data-attribute-id')] = $(obj).val();}
                    }
                });
                selectedAttributeValues = selectedAttributeValues.filter(function (el) { return el != null; });
                $('#product_attribute_values').val(selectedAttributeValues);
                loadProductAttributes();
            });

            $("body").on("change", '.product_attribute_value', function (e) {
                selectedAttributeValues = [];
                productAttributes = $('#product_attributes').val();
                $('.product_attribute_value').each(function(i, obj) {
                    if($(obj).val()){
                        if(productAttributes.includes($(obj).attr('data-attribute-id'))){ selectedAttributeValues[$(obj).attr('data-attribute-id')] = $(obj).val();}
                    }
                });
                selectedAttributeValues = selectedAttributeValues.filter(function (el) { return el != null; });
                $('#product_attribute_values').val(selectedAttributeValues);
            });

            var ckeditorToolbar = {
                // Define the toolbar groups as it is a more accessible solution.
                toolbarGroups: [
                    { "name": "basicstyles", "groups": ["basicstyles"] },
                    { "name": "links", "groups": ["links"] },
                    { "name": "paragraph", "groups": ["list", "blocks"] },
                    { "name": "document", "groups": ["mode"] },
                    { "name": "insert", "groups": ["insert"] },
                    { "name": "styles", "groups": ["styles"] },
                    { "name": "about", "groups": ["about"] }
                ],
                // Remove the redundant buttons from toolbar groups defined above.
                removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
            };

            CKEDITOR.replace('short_description', ckeditorToolbar);
            CKEDITOR.replace('description', ckeditorToolbar);

            $('body').on('keyup', '.variant_name', function (e) {
                if($(this).attr('data-product-id') == {{ $product->id }}){
                    $('#name').val($(this).val());
                }
            });

            $('body').on('keyup', '.variant_sku', function (e) {
                if($(this).attr('data-product-id') == {{ $product->id }}){
                    $('#sku').val($(this).val());
                }
            });

        });

        function createJSTree(jsondata) {
            var sortType = "asc";
            $('#categoriesTree').on('ready.jstree', (e, data) => {
                data.instance.sort = () => {};
            });
            $('#categoriesTree').jstree({
                "core": { "check_callback" : true, 'data': jsondata },
                "checkbox" : {
                    "keep_selected_style" : false,
                    three_state: false,
                    // cascade: 'up'
                },
                "plugins" : [ "search", "sort", "types", "checkbox" ],
                sort : function(a, b) {
                    a1 = this.get_node(a);
                    b1 = this.get_node(b);
                    if (sortType === "asc"){ return (a1.data.sort > b1.data.sort) ? 1 : -1; }
                    else { return (a1.data.sort > b1.data.sort) ? -1 : 1; }
                }
            });
            // console.log('CREATED', jsondata);
            categories = [];
            jsondata.forEach(el => {
                if(el.state.selected === true && !excludeCategories.includes(el.id)){
                    var id = el.id;
                    if(id) categories[id] = id;
                }
            });
            $('#categories').val(JSON.stringify(categories));
            // console.log('Categories', categories, JSON.stringify(categories));
        }

        function loadProductVariants(addNew = 0){

            variants = [];

            $('.product_variant').each(function(i, obj) {
                var pId = $(obj).attr('data-product-id');
                variant = {};
                variant.pId = pId;
                variant.sku = $('#variant_sku_'+pId).val() || '';
                variant.name = $('#variant_name_'+pId).val() || '';
                variant.price = $('#variant_price_'+pId).val() || '0';
                variant.stock = $('#variant_stock_'+pId).val() || '';
                variant.attributes = {};
                $('#variant_attributes').val().forEach(function(attr, x) {
                    // if($('#product_attribute_' + attr).val()) console.log('ssss', $('#product_attribute_' + attr));
                    variant.attributes[attr] = $('#product_'+pId+'_attribute_' + attr).val() || null;
                });
                variants.push(variant);
            });
            console.log($('.product_variant').length, variants);
            $('#variants').val(JSON.stringify(variants));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/products/loadProductVariants/{{ $product->id }}',
                method: 'post',
                data: {
                    variants: variants,
                    addNew: addNew
                    // variants: [{'pid':1, attributes: {'1':2, '2':4}},{'pid':2, attributes: {'1':2, '2':4}}],
                },
                success: function (data) {
                    $('#productVariants').html(data.productVariantsView);
                    // console.log('d: ', data.request)
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function loadProductAttributes(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: '/admin/products/loadProductAttributes/{{ $product->id }}',
                method: 'get',
                data: {
                    productAttributes: productAttributes,
                    productAttributeValues: selectedAttributeValues,
                },
                success: function (data) {
                    $('#productAttributes').html(data.productAttributes);
                    console.log(data.request)
                },
                error: function (data) {
                    alert('Error!');
                }
            });
        }

        function setFormData(){
            variants = [];

            $('.product_variant').each(function(i, obj) {
                var pId = $(obj).attr('data-product-id');
                variant = {};
                variant.pId = pId;
                variant.sku = $('#variant_sku_'+pId).val() || '';
                variant.name = $('#variant_name_'+pId).val() || '';
                variant.price = $('#variant_price_'+pId).val() || '0';
                variant.stock = $('#variant_stock_'+pId).val() || '';
                variant.attributes = {};
                $('#variant_attributes').val().forEach(function(attr, x) {
                    // if($('#product_attribute_' + attr).val()) console.log('ssss', $('#product_attribute_' + attr));
                    variant.attributes[attr] = $('#product_'+pId+'_attribute_' + attr).val() || null;
                });
                variants.push(variant);
            });
            $('#variants').val(JSON.stringify(variants));

            $('.pv').removeClass('hasError');
            var hasError = false;
            var vSKUs = {};
            var vNames = {};
            $('.product_variant').each(function(i, obj) {
                var vId = $(obj).attr('data-product-id');
                vSKUs[vId] = $('#variant_sku_'+vId).val();
                vNames[vId] = $('#variant_name_'+vId).val();
            });
            console.log(vSKUs,vNames);
            $('.product_variant').each(function(i, obj) {
                var pId = $(obj).attr('data-product-id');
                var checkSKUs = []; Object.keys(vSKUs).forEach(function (k){ if(k != pId) checkSKUs[k] = vSKUs[k]; });
                var checkNames = []; Object.keys(vNames).forEach(function (k){ if(k != pId) checkNames[k] = vNames[k]; });
                console.log('check', checkSKUs/*, checkNames*/);
                if(!$('#variant_sku_'+pId).val() || checkSKUs.includes($('#variant_sku_'+pId).val())){ hasError = true; $('#variant_sku_'+pId).addClass('hasError'); }
                if(!$('#variant_name_'+pId).val()/* || checkNames.includes($('#variant_name_'+pId).val())*/){ hasError = true; $('#variant_name_'+pId).addClass('hasError'); }
                if(Number($('#variant_price_'+pId).val()) <= 0.009){ hasError = true; $('#variant_price_'+pId).addClass('hasError'); }
                if(Number($('#variant_stock_'+pId).val()) < 0){ hasError = true; $('#variant_stock_'+pId).addClass('hasError'); }
            });
            if(!hasError){
                $('#submitCreateProductForm').trigger('click');
            } else {
                $("#dialog").html('<div class="alert alert-warning">Fix variants data and try again!</div>');
                $("#dialog").dialog();
            }
        }
    </script>
@endpush
