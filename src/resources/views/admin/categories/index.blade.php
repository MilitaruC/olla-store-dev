@extends('os_views::admin.layouts.default')
@section('title', 'Categories')
@section('page-title', 'Categories')
@section('content')
    <div class="d-flex flex-row modelControls">
        <a href="{{ url('/admin/categories/create') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-plus"></i> Add</a>
        <a href="#" class="btn btn-sm btn-primary categoriesTree"><i class="fa-solid fa-folder-tree"></i> Categories tree</a>
    </div>
    <div class="row card-white">
        <div class="col-md-4 leftCol reorderCategories hidden">
            <br>
            <h6>Reorder Categories</h6>
            <div id="categoriesTree"></div>
        </div>
        <div class="col-md-12 rightCol">
            <br>
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 100%">
                    {!! \Session::get('success') !!}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="d-flex flex-row searchInputs">
                <select name="perPage" id="perPage" class="form-control border-light-blue me-2">
                    <option value="5">5</option>
                    <option value="10" selected>10</option>
                    <option value="15">15</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <select name="sortBy" id="sortBy" class="form-control border-light-blue me-2">
                    <option value="categories.id">ID</option>
                    <option value="translations.name">Name</option>
                </select>
                <select name="sortOrder" id="sortOrder" class="form-control border-light-blue me-2">
                    <option value="DESC">DESC</option>
                    <option value="ASC">ASC</option>
                </select>
                <select name="activeStatus" id="activeStatus" class="form-control border-light-blue me-2">
                    <option value="">Active/Inactive</option>
                    <option value="true">Active</option>
                    <option value="false">Inactive</option>
                </select>
                <input type="text" name="search" id="search" class="form-control border-light-blue" placeholder="Search">
            </div>
            <div class="d-flex flex-row" id="categoriesAjaxList"></div>
        </div>

    </div>
@endsection
@push('after-styles')
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>
@endpush
@push('after-scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
<script>

$(document).ready(function() {

    var jsondata = {!! $json !!} ;
    createJSTree(jsondata);

    $('body').on('click', '.categoriesTree', function (e){
        var isHidden = $('.leftCol').hasClass('hidden');
        if(isHidden){
            $('.leftCol').removeClass('hidden');
            $('.rightCol').removeClass('col-md-12');
            $('.rightCol').addClass('col-md-8');
        } else {
            $('.leftCol').addClass('hidden');
            $('.rightCol').addClass('col-md-12');
            $('.rightCol').removeClass('col-md-8');
        }
    });

    function createJSTree(jsondata) {
        var sortType = "asc";
        $('#categoriesTree').on('ready.jstree', (e, data) => {
            data.instance.sort = () => {};
        });
        $('#categoriesTree').jstree({
            "core": {
                "check_callback" : true,
                'data': jsondata
            },
            "plugins" : [
                "dnd", "search", "sort", "types", "wholerow"
            ],
            "dnd": {
                check_while_dragging: true
            },
            sort : function(a, b) {
                a1 = this.get_node(a);
                b1 = this.get_node(b);
                if (sortType === "asc"){
                    return (a1.data.sort > b1.data.sort) ? 1 : -1;
                } else {
                    return (a1.data.sort > b1.data.sort) ? -1 : 1;
                }
            }
        }).bind("move_node.jstree", function (op, node, par, pos, more) {
            console.log($('#categoriesTree').data().jstree.get_json()[0].children);
            var categoriesTreeSortOrder = [];
            $('#categoriesTree').data().jstree.get_json()[0].children.forEach(function (el,k0){
                if(el.hasOwnProperty('id')){
                    categoriesTreeSortOrder.push({id: el.id, parent_id: null, sort: k0})
                    if(el.children.length > 0){
                        el.children.forEach(function (el1,k1){
                            if(el1.hasOwnProperty('id')){
                                categoriesTreeSortOrder.push({id: el1.id, parent_id: el.id, sort: k1})
                                if(el1.children.length > 0){
                                    el1.children.forEach(function (el2,k2){
                                        if(el2.hasOwnProperty('id')){
                                            categoriesTreeSortOrder.push({id: el2.id, parent_id: el1.id, sort: k2})
                                            if(el2.children.length > 0){
                                                el2.children.forEach(function (el3,k3){
                                                    if(el3.hasOwnProperty('id')){
                                                        categoriesTreeSortOrder.push({id: el3.id, parent_id: el2.id, sort: k3})
                                                        if(el2.children.length > 0){

                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });

            $.ajax({
                url: '/admin/categories/reorder',
                method: 'post',
                data: {
                    categoriesTreeSortOrder: categoriesTreeSortOrder
                },
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            if(more && more.dnd) {
                return more.pos !== "i" && par.id == node.parent;
            }
            return true;
        }).on('loaded.jstree', function() {
            $('#categoriesTree').jstree('open_all');
        });
    }

    let model = {
        name: 'category',
        names: 'categories',
        Name: 'Category',
        Names: 'Categories',
        urlSearch: '/admin/categories/ajax-list',
        urlStore: '/admin/categories/store',
        urlUpdate: '/admin/categories/update/',
        urlUpdateStatus: '/admin/categories/update/active-status/',
        urlDestroy: '/admin/categories/destroy/',
        urlLoadVendorStores: '/admin/categories/load-vendor-stores',
        urlLoadVendorStoresCategories: '/admin/categories/load-vendor-stores-categories',
        sendData: {
            perPage: 10,
            page: '1',
            sortBy: 'id',
            sortOrder: 'DESC',
            search: '',
            activeStatus: null,
            storeID: null,
        }
    };

    filterModel();

    $(document).on('click', '.pagination a', function(e) {
        e.preventDefault();

        let url = new URL($(this).attr('href'));
        let page = url.searchParams.get('page');

        model.sendData.page = page;

        // var url = $(this).attr('href');
        filterModel();
        // window.history.pushState("", "", url);
    });
    $(document).on('keyup', '#search', function (e) {
        model.sendData.page = '1';
        clearTimeout(timeout);
        timeout = setTimeout(function () { filterModel(); }, 500);
    });
    $(document).on('change', '#perPage', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#sortBy', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#sortOrder', function (e) {
        model.sendData.page = '1';
        filterModel();
    });
    $(document).on('change', '#activeStatus', function (e) {
        model.sendData.page = '1';
        filterModel();
    });

    var timeout = null;

    function filterModel(page = null)
    {
        if(page) model.sendData.page = page;
        model.sendData.search = $('#search').val();
        model.sendData.perPage = $('#perPage').val();
        model.sendData.sortBy = $('#sortBy').val();
        model.sendData.sortOrder = $('#sortOrder').val();
        model.sendData.activeStatus = $('#activeStatus').val();
        model.sendData.storeID = $('#storeID').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: model.urlSearch,
            method: 'post',
            data: model.sendData,
            success: function (data) {
                $('#'+model.names+'AjaxList').html(data);
            },
            error: function (data) {
                alert('Error!');
            }
        });
    }
});
</script>
@endpush
