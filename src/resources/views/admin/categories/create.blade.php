@extends('os_views::admin.layouts.default')
@section('title', 'Add new category')
@section('page-title', 'Add new category')
@section('content')
    <div class="d-flex flex-row modelControls">
        <a href="{{ url('/admin/categories') }}" class="btn btn-sm btn-primary"><i class="fa-solid fa-chevron-left"></i> Back</a>
    </div>

    <div class="row card-white">
        <div class="col-md-12 p-4 modelSection"><h3>Info</h3></div>
        @if ($errors->any())
            <div class="col-md-12 p-4">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        @if (\Session::has('fail'))
            <div class="col-md-12 p-4">
                <div class="alert alert-warning" style="width: 100%">
                    <ul>
                        <li>{!! \Session::get('fail') !!}</li>
                    </ul>
                </div>
            </div>
        @endif
        <form action="{{ url('/admin/categories/store') }}" method="post">
            <div class="col-md-12 p-4">
                @csrf
                <div class="mb-3 form-check form-check-inline">
                    <input type="checkbox" name="active" class="form-check-input border-light-blue" id="active">
                    <label class="form-check-label" for="active">Active</label>
                </div>
                <div class="mb-3">
                    <label for="parent_id" class="form-label selectParent"><i class="fa-solid fa-angle-down"></i><i class="fa-solid fa-angle-up hidden"></i> Parent category: <span>{{ ($category->parent->name ?? 'None selected') }}</span></label>
                    <input type="text" class="form-control border-light-blue hidden" name="parent_id" id="parent_id" value="{{ (old('parent_id') ?? '') }}">
                    <div id="categoriesTree" class="hidden"></div>
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control border-light-blue" name="name" id="name" aria-describedby="nameHelp" value="{{ old('name') }}">
                    {{--<div id="nameHelp" class="form-text">We'll never share your email with anyone else.</div>--}}
                </div>
                <div class="mb-3">
                    <label for="short_description" class="form-label">Short description</label>
                    <textarea class="form-control border-light-blue" name="short_description" id="short_description" rows="10">{{ old('short_description') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea class="form-control border-light-blue" name="description" id="description" rows="3">{{ old('description') }}</textarea>
                </div>
            </div>

            <div class="col-md-12 p-4 modelSection" data-section="categorySeo"><h3>SEO</h3></div>
            <div class="col-md-12 p-4 categorySeo">
                <div class="mb-3">
                    <label for="url" class="form-label">Url</label>
                    <input type="text" class="form-control border-light-blue" name="url" id="url" aria-describedby="urlHelp" value="{{ old('url') }}">
                    {{--            <div id="urlHelp" class="form-text">We'll never share your email with anyone else.</div>--}}
                </div>
                <div class="mb-3">
                    <label for="meta_title" class="form-label">Meta title</label>
                    <input type="text" class="form-control border-light-blue" name="meta_title" id="meta_title" aria-describedby="meta_titleHelp" value="{{ old('meta_title') }}">
                    {{--            <div id="meta_titleHelp" class="form-text">We'll never share your email with anyone else.</div>--}}
                </div>
                <div class="mb-3">
                    <label for="meta_key" class="form-label">Meta keys</label>
                    <textarea class="form-control border-light-blue" name="meta_key" id="meta_key" rows="1">{{ old('meta_key') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="meta_desc" class="form-label">Meta description</label>
                    <textarea class="form-control border-light-blue" name="meta_desc" id="meta_desc" rows="2">{{ old('meta_desc') }}</textarea>
                </div>

            </div>
            <button type="submit" class="btn btn-primary hidden" id="submitCreateCategoryForm">Submit</button>
        </form>
    </div>
    <button type="button" class="btn btn-primary saveModel" onclick="$('#submitCreateCategoryForm').trigger('click');">Save</button>

@endsection
@push('after-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>
@endpush
@push('after-scripts')
    <script src="{{ asset('os_assets/admin/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script>
        $(document).ready(function () {

            var jsondata = {!! $json !!} ;

            createJSTree(jsondata);

            $('body').on('click', '#categoriesTree .jstree-anchor', function (e){
                e.stopPropagation();
                console.log($(this).parent().attr('id'),$(this).text());
                var parentId = $(this).parent().attr('id');
                var parentName = $(this).text();
                if(parentName == 'Store'){ parentName = 'None selected'; parentId = 0; }
                $('#parent_id').val(parentId);
                $('.selectParent span').text(parentName);
            });
            $('body').on('click', '.selectParent', function (e){
                $('#categoriesTree').toggleClass('hidden');
                $('.selectParent .fa-angle-down').toggleClass('hidden');
                $('.selectParent .fa-angle-up').toggleClass('hidden');
            });

            $(document).on('click', '.jstree-icon', function(e, data) {

            });

            function createJSTree(jsondata) {
                var sortType = "asc";
                $('#categoriesTree').on('ready.jstree', (e, data) => {
                    data.instance.sort = () => {};
                });
                $('#categoriesTree').jstree({
                    "core": { "check_callback" : true, 'data': jsondata },
                    "plugins" : [ "search", "sort", "types" ],
                    sort : function(a, b) {
                        a1 = this.get_node(a);
                        b1 = this.get_node(b);
                        if (sortType === "asc"){ return (a1.data.sort > b1.data.sort) ? 1 : -1; }
                        else { return (a1.data.sort > b1.data.sort) ? -1 : 1; }
                    }
                }).on('loaded.jstree', function() { $('#categoriesTree').jstree('open_all'); });
            }

            var ckeditorToolbar = {
                // Define the toolbar groups as it is a more accessible solution.
                toolbarGroups: [
                    { "name": "basicstyles", "groups": ["basicstyles"] },
                    { "name": "links", "groups": ["links"] },
                    { "name": "paragraph", "groups": ["list", "blocks"] },
                    { "name": "document", "groups": ["mode"] },
                    { "name": "insert", "groups": ["insert"] },
                    { "name": "styles", "groups": ["styles"] },
                    { "name": "about", "groups": ["about"] }
                ],
                // Remove the redundant buttons from toolbar groups defined above.
                removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,PasteFromWord'
            };

            CKEDITOR.replace('short_description', ckeditorToolbar);
            CKEDITOR.replace('description', ckeditorToolbar);

        });
    </script>
@endpush
