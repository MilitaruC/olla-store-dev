<div class="d-flex flex-column flex-fill bd-highlight mb-3">
    <div class="d-flex justify-content-between pagination-wrapper">
        @if(count($categories)) {{ $categories->links()}} @endif
        <div class="modelTotals">{{ $categories->count()}} / {{ $categories->total()}} </div>
    </div>
    <table class="table table-striped table-hover">
        <thead class="table-header-light-blue">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Active</th>
            <th scope="col">Products</th>
            <th scope="col">Updated</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <th scope="row" style="width:30px;">{{ $category->id }}</th>
            <td>{{ $category->name }}</td>
            <td>@if($category->active == 1) <i class="fa-solid fa-square-check" style="color: #2473bf"></i> @else <i class="fa-solid fa-square" style="color: #535353"></i> @endif</td>
            <td>{{ $category->products_count }}</td>
            <td>{{ $category->updated_at }}</td>
            <td style="width: 100px"><a href="{{ url('/admin/categories/edit/'.$category->id) }}" class="btn btn-sm btn-success"><i class="fa-solid fa-square-pen"></i></a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <div class="panel-controls pull-right">
        @if(count($categories)) {{ $categories->links()}} @endif
    </div>
</div>
