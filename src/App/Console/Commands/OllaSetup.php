<?php

namespace Militaruc\OllaStore\App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Militaruc\OllaStore\App\Support\HLP;

class OllaSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'olla:setup {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Olla-Store Setup. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $vendorPath = '/vendor/militaruc/olla-store-dev/src';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->vendorPath = olla()->getVendorRelativePath();

        $force = (new ArgvInput())->hasParameterOption('--force');

        $this->info('Checking setup...');
        if ($this->confirm('You need to have your correct DB credentials in .env file first. Do you wish to continue?')) {
            if ($this->confirm('Make a DB backup if you need. Migrations will also be deleted. Do you wish to continue?')) {
                $this->info('Remove existing setup files...');
                array_map('unlink', glob(base_path() . "/database/migrations/*"));
                //(file_exists(app_path() . "config/olla.php")) ? unlink(app_path() . "config/olla.php") : null;
                //(file_exists(app_path() . "config/translatable.php")) ? unlink(app_path() . "config/translatable.php") : null;
                $this->info('Olla-Store setup removed successfully');
            }
            if (!file_exists(base_path() . $this->vendorPath . '/config/auth.php') || (file_exists(base_path() . $this->vendorPath . '/config/auth.php') && $force)) {
                if (!copy(base_path() . $this->vendorPath . '/config/auth.php', base_path() . '/config/auth.php')) {
                    $this->info("File config/auth.php can't be copied!");
                } else {
                    $this->info("File config/auth.php has been copied!");
                }
            }
            if (!file_exists(base_path() . $this->vendorPath . '/config/olla.php') || (file_exists(base_path() . $this->vendorPath . '/config/olla.php') && $force)) {
                if (!copy(base_path() . $this->vendorPath . '/config/olla.php', base_path() . '/config/olla.php')) {
                    $this->info("File config/olla.php can't be copied!");
                } else {
                    $this->info("File config/olla.php has been copied!");
                }
            }
            if (!file_exists(base_path() . $this->vendorPath . '/config/translatable.php') || (file_exists(base_path() . $this->vendorPath . '/config/translatable.php') && $force)) {
                if (!copy(base_path() . $this->vendorPath . '/config/translatable.php', base_path() . '/config/translatable.php')) {
                    $this->info("File config/translatable.php can't be copied!");
                } else {
                    $this->info("File config/translatable.php has been copied!");
                }
            }
            if (!file_exists(base_path() . $this->vendorPath . '/config/permission.php') || (file_exists(base_path() . $this->vendorPath . '/config/permission.php') && $force)) {
                if (!copy(base_path() . $this->vendorPath . '/config/permission.php', base_path() . '/config/permission.php')) {
                    $this->info("File config/permission.php can't be copied!");
                } else {
                    $this->info("File config/permission.php has been copied!");
                }
            }

            if (!file_exists(base_path() . $this->vendorPath . '/routes/web.php') || (file_exists(base_path() . $this->vendorPath . '/routes/web.php') && $force)) {
                if ($this->confirm('You routes/web.php will be replaced. Do you wish to continue?')) {
                    if (!copy(base_path() . $this->vendorPath . '/routes/web.php', base_path() . '/routes/web.php')) {
                        $this->info("File routes/web.php can't be copied!");
                    } else {
                        $this->info("File routes/web.php has been copied!");
                    }
                }
            }

            if ($this->confirm('Middleware\Authenticate.php will be replaced. Do you wish to continue?')) {
                if (!copy(base_path() . $this->vendorPath . '/App/Http/Middleware/Authenticate.php', base_path() . '/app/Http/Middleware/Authenticate.php')) {
                    $this->info("File Middleware/Authenticate.php can't be copied!");
                } else {
                    $this->info("File Middleware/Authenticate.php has been copied!");
                }
            }

            if ($this->confirm('Middleware\RedirectIfAuthenticated.php will be replaced. Do you wish to continue?')) {
                if (!copy(base_path() . $this->vendorPath . '/App/Http/Middleware/RedirectIfAuthenticated.php', base_path() . '/app/Http/Middleware/RedirectIfAuthenticated.php')) {
                    $this->info("File Middleware/RedirectIfAuthenticated.php can't be copied!");
                } else {
                    $this->info("File Middleware/RedirectIfAuthenticated.php has been copied!");
                }
            }

            HLP::copy_directory(base_path() . $this->vendorPath . '/resources/assets', public_path() . '/os_assets');
            $this->info("Assets has been copied to public/os_assets!");

            $this->call('config:clear');
            $this->call('migrate:fresh');
            $this->call('vendor:publish', ['--provider' => 'Militaruc\OllaStore\App\Providers\OllaStoreServiceProvider --force']);
            $this->call('vendor:publish', ['--provider' => 'Spatie\Permission\PermissionServiceProvider']);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\AdminSeeder"]);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\CustomerSeeder"]);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\ProductTypesSeeder"]);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\AttributeSeeder"]);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\CategoriesSeeder"]);
            $this->call('db:seed', ['--class' => "Militaruc\OllaStore\database\seeders\ProductSeeder"]);
            $this->call('optimize');
            array_map('unlink', glob(base_path() . "/database/migrations/*"));

            $this->info('Olla-Store setup completed!');

        } else {
            $this->info('Olla-Store setup aborted!');
        }

            //$params = [
            //    '--class' => "Militaruc\OllaStore\database\seeders\UserSeeder",
            //];
            //$this->call('php artisan vendor:publish', ['--provider' => 'Spatie\Permission\PermissionServiceProvider']);
            //$this->call('config:clear');
            //$this->call('migrate:fresh', $params);
            //$this->call('db:seed', $params);
            //
            //$this->call('optimize');




        // php artisan db:seed --class=CustomerSeeder
    }
}
