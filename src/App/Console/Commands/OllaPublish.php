<?php

namespace Militaruc\OllaStore\App\Console\Commands;

use Illuminate\Console\Command;
use Militaruc\OllaStore\App\Support\HLP;
use Symfony\Component\Console\Input\ArgvInput;

class OllaPublish extends Command
{
    protected $files = [
        'Config' => [
            'auth' => [
                'from' => '/config/auth.php',
                'to_dir' => '/config',
                'to' => '/config/auth.php'
            ],
            'olla' => [
                'from' => '/config/olla.php',
                'to_dir' => '/config',
                'to' => '/config/olla.php'
            ],
            'permission' => [
                'from' => '/config/permission.php',
                'to_dir' => '/config',
                'to' => '/config/permission.php'
            ],
            'setup' => [
                'from' => '/config/setup.php',
                'to_dir' => '/config',
                'to' => '/config/setup.php'
            ],
            'translatable' => [
                'from' => '/config/translatable.php',
                'to_dir' => '/config',
                'to' => '/config/translatable.php'
            ],
        ],
        'Controllers' => [
            'OllaHomeController' => [
                'from' => '/App/Http/Controllers/OllaHomeController.php',
                'to_dir' => '/app/Http/Controllers',
                'to' => '/app/Http/Controllers/OllaHomeController.php'
            ],
            'Auth/OllaLoginController' => [
                'from' => '/App/Http/Controllers/Auth/OllaLoginController.php',
                'to_dir' => '/app/Http/Controllers/Auth',
                'to' => '/app/Http/Controllers/Auth/OllaLoginController.php'
            ],
            'Admin/OllaDashboardController' => [
                'from' => '/App/Http/Controllers/Admin/OllaDashboardController.php',
                'to_dir' => '/app/Http/Controllers/Admin',
                'to' => '/app/Http/Controllers/Admin/OllaDashboardController.php'
            ],
            'Admin/OllaCategoryController' => [
                'from' => '/App/Http/Controllers/Admin/OllaCategoryController.php',
                'to_dir' => '/app/Http/Controllers/Admin',
                'to' => '/app/Http/Controllers/Admin/OllaCategoryController.php'
            ],
            'Admin/Auth/OllaAdminLoginController' => [
                'from' => '/App/Http/Controllers/Admin/Auth/OllaAdminLoginController.php',
                'to_dir' => '/app/Http/Controllers/Admin/Auth',
                'to' => '/app/Http/Controllers/Admin/Auth/OllaAdminLoginController.php'
            ],
        ],
        'Models' => [
            'User' => [
                'from' => '/App/Models/User.php',
                'to_dir' => '/app/Models',
                'to' => '/app/Models/User.php'
            ],
            'Category' => [
                'from' => '/App/Models/Category.php',
                'to_dir' => '/app/Models',
                'to' => '/app/Models/Category.php'
            ],
            'CategoryTranslation' => [
                'from' => '/App/Models/CategoryTranslation.php',
                'to_dir' => '/app/Models',
                'to' => '/app/Models/CategoryTranslation.php'
            ],
        ],
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'olla:publish {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Olla-Store Setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $vendorPath = '/vendor/militaruc/olla-store/src';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->vendorPath = olla()->getVendorPath();

        $force = (new ArgvInput())->hasParameterOption('--force');

        $defaultIndex = 0;
        $choiceToPublish = ['All'];
        $toPublish = $this->choice('What do you want to publish?',
            [
                'All',
                'Config',
                'Assets',
                'Views',
                'Controllers',
                'Models',
                'Migrations',
                'Translations',
            ],
            $defaultIndex,
            $maxAttempts = null,
            $allowMultipleSelections = false);

        if($toPublish == 'Config'){
            $choiceToPublish = $this->choice(
                'Choose controller (use , delimiter to make a selection, ex: 1,3)',
                [
                    'All',
                    'auth',
                    'olla',
                    'permission',
                    'setup',
                    'translatable',
                ],
                $defaultIndex,
                $maxAttempts = null,
                $allowMultipleSelections = true);
            if(in_array('All', $choiceToPublish)){
                foreach ($this->files['Config'] as $configName=>$config){
                    if(!file_exists(base_path().$config['to']) || (file_exists(base_path().$config['to']) && $force)){
                        if (!is_dir(base_path() . $config['to_dir'])) {
                            mkdir(base_path() . $config['to_dir'], 0775, true);
                        }
                        if( !copy($this->vendorPath . $config['from'], base_path().$config['to']) ) {
                            $this->info("File $configName can't be copied!");
                        }
                        else {
                            $this->info("File $configName has been copied!");
                        }
                    }
                }
            }
        }

        if($toPublish == 'Controllers'){
            $choiceToPublish = $this->choice(
                'Choose controller (use , delimiter to make a selection, ex: 1,3)',
                [
                    'All',
                    'OllaHomeController',
                    'Auth/OllaLoginController',
                    'Admin/OllaDashboardController',
                    'Admin/OllaCategoryController',
                    'Admin/Auth/OllaAdminLoginController',
                ],
                $defaultIndex,
                $maxAttempts = null,
                $allowMultipleSelections = true);
            if(in_array('All', $choiceToPublish)){
                foreach ($this->files['Controllers'] as $controllerName=>$controller){
                    if(!file_exists(base_path().$controller['to']) || (file_exists(base_path().$controller['to']) && $force)){
                        if (!is_dir(base_path() . $controller['to_dir'])) {
                            mkdir(base_path() . $controller['to_dir'], 0775, true);
                        }
                        if( !copy($this->vendorPath . $controller['from'], base_path().$controller['to']) ) {
                            $this->info("File $controllerName can't be copied!");
                        }
                        else {
                            $this->info("File $controllerName has been copied!");
                        }
                    }
                }
            }
        }
        if($toPublish == 'Models'){
            $choiceToPublish = $this->choice(
                'Choose model (use , delimiter to make a selection, ex: 1,3)',
                [
                    'All',
                    'User',
                    'Category',
                    'CategoryTranslation'
                ],
                $defaultIndex,
                $maxAttempts = null,
                $allowMultipleSelections = true);
            if(in_array('All', $choiceToPublish)){
                foreach ($this->files['Models'] as $modelName=>$model){
                    if(!file_exists($model['to']) || (file_exists(base_path().$model['to']) && $force)){
                        if (!is_dir(base_path() . $model['to_dir'])) {
                            mkdir(base_path() . $model['to_dir'], 0775, true);
                        }
                        if( !copy($this->vendorPath . $model['from'], base_path().$model['to']) ) {
                            $this->info("File $modelName can't be copied!");
                        }
                        else {
                            $this->info("File $modelName has been copied!");
                        }
                    }
                }
            }
            else {
                if (in_array('User', $choiceToPublish)){
                    if(!file_exists($this->files['Models']['User']['to']) || (file_exists(base_path().$this->files['Models']['User']['to']) && $force)){
                        if (!is_dir(base_path() . $this->files['Models']['User']['to_dir'])) {
                            mkdir(base_path() . $this->files['Models']['User']['to_dir'], 0775, true);
                        }
                        if( !copy($this->vendorPath . $this->files['Models']['User']['from'], base_path().$this->files['Models']['User']['to']) ) {
                            $this->info("File User.php can't be copied!");
                        }
                        else {
                            $this->info("File User.php has been copied!");
                        }
                    }
                }
            }
        }
        if($toPublish == 'Assets'){
            $choiceToPublish = $this->choice(
                'Choose assets group (use , delimiter to make a selection, ex: 1,3)',
                [
                    'All',
                    'Admin',
                    'Frontend'
                ],
                $defaultIndex,
                $maxAttempts = null,
                $allowMultipleSelections = true);
            if(in_array('All', $choiceToPublish)){
                HLP::copy_directory($this->vendorPath . '/resources/assets', public_path().'/os_assets', $force);
                $this->info("Assets has been copied to public/os_assets!");
            }
        }
        if($toPublish == 'Translations'){
            HLP::copy_directory($this->vendorPath . '/resources/lang', base_path().'/resources/lang', $force);
            $this->info("Translations has been copied to resources/lang!");
        }

        $this->info('Publishing complete');
        !$force ? $this->info('Use --force if you want to overwrite the existing files') : null;

    }
}
