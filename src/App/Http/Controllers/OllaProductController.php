<?php

namespace Militaruc\OllaStore\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Militaruc\OllaStore\App\Models\Product;

class OllaProductController extends Controller
{
    public function index(Request $request, $slug)
    {
        $product = Product::with('translations')
            ->whereHas('translations', function ($query) use($slug){
                $query->where('url', $slug);
            })
            ->with('images')
            ->first();

        //dd(cart()->get());

        $data = [
            'product' => $product,
            //'breadcrumbs' => array_reverse(hlp()->categoryParents($category->id)),
            'cartData' => cart()->get(),
            'request' => $request,
        ];

        return view('os_views::frontend.products.index', $data);
    }
}
