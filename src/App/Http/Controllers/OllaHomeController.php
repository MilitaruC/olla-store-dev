<?php

namespace Militaruc\OllaStore\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Militaruc\OllaStore\App\Models\Category;
use Militaruc\OllaStore\App\Models\Customer;

class OllaHomeController extends Controller
{
    public function index(Request $request)
    {
        //dump($request->session()->all());
        $categories = Category::with('translations')->get();

        $data = [
            'cartData' => cart()->get(),
        ];

        return view('os_views::frontend.home.index', $data);
    }
}
