<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Militaruc\OllaStore\App\Models\Attribute;
use Militaruc\OllaStore\App\Models\AttributeValue;
use Militaruc\OllaStore\App\Models\Category;
use Militaruc\OllaStore\App\Models\Image as ProductImage;
use Militaruc\OllaStore\App\Models\Product;
use HLP;
use Militaruc\OllaStore\App\Models\ProductTranslation;
use Militaruc\OllaStore\App\Models\ProductType;
use Image;

class OllaProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $categories = Category::all();
        return view('os_views::admin.products.index', ['categories' => $categories]);
    }

    public function ajaxList(Request $request)
    {
        $perPage = '10';
        $sortBy = 'products.id';
        $sortOrder = 'desc';
        $search = ($request->search ?? '');
        $search = trim($search);
        $searches = explode(' ', $search);
        $active = '';

        if(($request->perPage ?? null)) $perPage = $request->perPage;
        if(($request->sortBy ?? null)) $sortBy = $request->sortBy;
        if(($request->sortOrder ?? null)) $sortOrder = $request->sortOrder;
        if(($request->activeStatus ?? null) == 'true' || ($request->activeStatus ?? null) == 'false') $active = $request->activeStatus;

        $products = Product::with('translations')
            ->when(($active ?? null), function($query) use ($active) {
                return $query->where('active', '=', ($active == 'true' ? '1' : '0'));
            })
            ->when($request->search, function($query) use ($request, $searches){
                return $query->whereHas('translations', function ($query) use ($request, $searches){
                    foreach($searches as $key => $search) {
                        if($key == '0'){
                            $query->where('name', 'like', '%'.$search.'%')
                                ->orWhere('short_description', 'like', '%'.$search.'%')
                                ->orWhere('description', 'like', '%'.$search.'%');
                        }else{
                            $query->orWhere('name', 'like', '%'.$search.'%')
                                ->orWhere('short_description', 'like', '%'.$search.'%')
                                ->orWhere('description', 'like', '%'.$search.'%');
                        }
                    }
                });
            })
            ->orderBy($sortBy, $sortOrder)
            ->paginate($perPage);

        $data = [
            'products' => $products,
        ];

        return view('os_views::admin.products.partials.list', $data)->render();
    }

    public function create()
    {
        $categories = Category::all();
        $attributes = Attribute::all();
        $productTypes = ProductType::where('active', 1)->get();

        $data = [
            'json' => hlp()::jsTreeCategories(/*[5,6]*/),
            'productTypes' => $productTypes,
            'attributes' => $attributes,
        ];

        return view('os_views::admin.products.create', $data);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $url = ($request->url ?? $request->name ?? null);
        $validator = \Validator::make($request->all(),[
            'sku' => ['required', 'unique_with:products,vendor_id'],
            'name' => ['required', 'min:3', 'max:191'],
            'type_id' => ['required', 'integer'],
            'price' => ['required', 'numeric'],
        ]);
        $url = $this->validateURL($url);
        $validator->after(function ($validator) use ($url) {
            if (!$url) {
                $validator->errors()->add('url', 'Url or automatic url generation is not unique for current lang: '.app()->getLocale().'!');
            }
        });

        if ($validator->fails()) {
            return redirect('admin/products/create')->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        try{
            $product = new Product();
            (($request->active ?? null) == 'on') ? $product->active = 1 : null;
            //(($request->parent_id ?? 0) > 0) ? $product->parent_id = $request->parent_id : $product->parent_id = null;
            $product->vendor_id = ($request->vendor_id ?? 1);
            $product->type_id = $request->type_id;
            $product->configurable = ($request->configurable ?? '0');
            $product->name = $request->name;
            $product->full_name = $request->name;
            $product->sku = $request->sku;
            $product->stock = $request->stock;
            $product->price = $request->price;
            $product->vat_price = vatFromGross($request->price, 19);
            $product->price_without_vat = priceWithoutVatFromGross($request->price, 19);
            $product->old_price = $request->old_price;
            $product->old_vat = vatFromGross($request->old_price, 19);
            $product->old_price_without_vat = priceWithoutVatFromGross($request->old_price, 19);
            $product->short_description = $request->short_description;
            $product->description = $request->description;
            $product->url = $url;
            $product->meta_title = $request->meta_title;
            $product->meta_key = $request->meta_key;
            $product->meta_desc = $request->meta_desc;
            $product->save();

            $product->categories()->syncWithoutDetaching(array_filter(json_decode($request->categories)));
            if($request->product_attributes && is_array($request->product_attributes)){
                if(count($request->product_attributes)){ $product->attributes()->sync($request->product_attributes); }
            }
            if($request->product_attribute_values && is_array($request->product_attribute_values)) {
                if (count($request->product_attribute_values)) {
                    $product->attributeValues()->sync($request->product_attribute_values);
                }
            }

            $file = ($request->file ?? null);
            // for save original image
            $ogImage = Image::make($file);
            $relativePath = '/media/products/' . $product->id . '/';
            $originalPath = public_path($relativePath);
            File::ensureDirectoryExists($originalPath);
            $ogImage = $ogImage->save($originalPath . $file->getClientOriginalName());

            $ogImage->resize(800, 800);
            $thImage = $ogImage->save($originalPath . '800x800-' . $file->getClientOriginalName());
            $ogImage->resize(600, 600);
            $thImage = $ogImage->save($originalPath . '600x600-' . $file->getClientOriginalName());

            // for store resized image
            $thumbnailPath = public_path('/media/products/' . $product->id . '/thumbnail/');
            File::ensureDirectoryExists($thumbnailPath);
            $ogImage->resize(310, 310);
            $thImage = $ogImage->save($thumbnailPath . '310x310-' . $file->getClientOriginalName());
            $ogImage->resize(100, 100);
            $thImage = $ogImage->save($thumbnailPath . '100x100-' . $file->getClientOriginalName());
            $ogImage->resize(60, 60);
            $thImage = $ogImage->save($thumbnailPath . '60x60-' . $file->getClientOriginalName());
            $newImage = new ProductImage();
            $newImage->model = get_class($product);
            $newImage->file = $file->getClientOriginalName();
            $newImage->rel_path = $relativePath;
            $newImage->path = $originalPath;
            $newImage->save();

            if ($product->configurable > 0) {
                $newImage->products()->attach([$product->id]);
            } else {
                $newImage->products()->attach([$product->id => ['pid' => $product->id]]);
            }

            DB::commit();
        } catch (\Exception $ex){dd($ex->getMessage(), $ex->getTrace());
            DB::rollBack();
            return redirect()->back()->with('fail', 'Ops, something went wrong!')->withErrors($validator)->withInput();
        }

        return redirect()->to('/admin/products')->with('success', 'Product created!');
    }

    public function edit($id)
    {
        $product = Product::where('id', $id)->with('images')->first();
        $productTypes = ProductType::where('active', 1)->get();
        $attributes = Attribute::all();

        $data = [
            'product' => $product,
            'json' => hlp()::jsTreeCategories($product->categories->pluck('id')->toArray()),
            'productTypes' => $productTypes,
            'attributes' => $attributes,
        ];

        if(!($product->id ?? null)){ return redirect()->back()->with('fail', 'Ops, requested product not found!'); }

        return view('os_views::admin.products.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();
        $parentID = ($product->has_parent ?? $product->id);
        $url = ($request->url ?? $request->name ?? null);

        DB::beginTransaction();

        try
        {
            if($request->configurable < 1)
            {
                $validator = \Validator::make($request->all(),[
                    'sku' => ['required'],
                    'name' => ['required', 'min:3', 'max:191'],
                    'type_id' => ['required', 'integer'],
                    'price' => ['required', 'numeric'],
                ]);
                $url = $this->validateURL($url, $id);
                $validator->after(function ($validator) use ($url, $product) {
                    if (!$url) {
                        $validator->errors()->add('url', 'Url or automatic url generation is not unique "'.$url.'" for current lang: '.app()->getLocale().'!');
                    }
                    $checkSku = Product::where('vendor_id', $product->vendor_id)->where('sku', $product->sku)->where('id', '!=', $product->id)->first();
                    if (($checkSku->id ?? null)) {
                        $validator->errors()->add('sku', 'You cannot change to an already exiting SKU!');
                    }
                });

                if ($validator->fails()) {
                    return redirect('admin/products/edit/'.$id)->withErrors($validator)->withInput();
                }

                (($request->active ?? null) == 'on') ? $product->active = 1 : null;
                //(($request->parent_id ?? 0) > 0) ? $product->parent_id = $request->parent_id : $product->parent_id = null;
                $product->vendor_id = ($request->vendor_id ?? 1);
                $product->type_id = $request->type_id;
                $product->configurable = ($request->configurable ?? '0');
                $product->name = $request->name;
                $product->full_name = $request->name;
                $product->sku = $request->sku;
                $product->stock = $request->stock;
                $product->price = $request->price;
                $product->vat_price = vatFromGross($request->price, 19);
                $product->price_without_vat = priceWithoutVatFromGross($request->price, 19);
                $product->old_price = $request->old_price;
                $product->old_vat = vatFromGross($request->old_price, 19);
                $product->old_price_without_vat = priceWithoutVatFromGross($request->old_price, 19);
                $product->short_description = $request->short_description;
                $product->description = $request->description;
                $product->url = $url;
                $product->meta_title = $request->meta_title;
                $product->meta_key = $request->meta_key;
                $product->meta_desc = $request->meta_desc;
                $product->save();

                $product->categories()->sync(array_filter(json_decode($request->categories)));//dd($request->all());

                if($request->product_attributes && is_array($request->product_attributes)){
                    if(count($request->product_attributes)){ $product->attributes()->sync($request->product_attributes); }
                }
                if($request->product_attribute_values && is_array($request->product_attribute_values)) {
                    if (count($request->product_attribute_values)) {
                        $product->attributeValues()->sync($request->product_attribute_values);
                    }
                }
            } else {
                /**
                 * MAKE BIG DATA VALIDATION
                 * PERSIST $request->variants and return to edit page OR MAKE AJAX VALIDATION WHEN saving VARIANTS and proceed when no ERRORS
                 * [SKU, NAME, URL, TYPE_ID, PRICE] VALIDATION FOR EVERY variant
                 */
                foreach(json_decode($request->variants) as $variant) {//dump(json_decode($request->variants),$variant);
                    $checkProduct = Product::where('id', $variant->pId)->first();
                    if($checkProduct->id ?? null){
                        $variantProduct = $checkProduct;
                    } else {
                        $variantProduct = $product->replicate();
                        $variantProduct->unsetRelation('translations');
                        $variantProduct->unsetRelation('attributes');
                        $variantProduct->unsetRelation('attributeValues');
                        $variantProduct->id = null;
                        $variantProduct->is_parent = 0;
                        $variantProduct->has_parent = $parentID;
                    }

                    $variantProduct->configurable = ($request->configurable ?? '0');
                    $variantProduct->type_id = $request->type_id;
                    $variantProduct->sku = $variant->sku;
                    $variantProduct->name = $variant->name;
                    $variantProduct->full_name = $variant->name;
                    $variantProduct->url = $this->validateURL($variant->name, $variantProduct->id);
                    $variantProduct->price = $variant->price;
                    $variantProduct->vat_price = vatFromGross($variant->price, 19);
                    $variantProduct->price_without_vat = priceWithoutVatFromGross($variant->price, 19);
                    $variantProduct->stock = $variant->stock;
                    $variantProduct->active = ($variant->active ?? 1);
                    $variantProduct->short_description = $request->short_description;
                    $variantProduct->description = $request->description;
                    $variantProduct->meta_title = $request->meta_title;
                    $variantProduct->meta_key = $request->meta_key;
                    $variantProduct->meta_desc = $request->meta_desc;
                    $variantProduct->save();

                    $variantProduct->categories()->sync(array_filter(json_decode($request->categories)));

                    if($variant->attributes && is_array((array) $variant->attributes)){
                        if(count((array) $variant->attributes)){
                            $variantProduct->attributes()->sync(array_keys((array) $variant->attributes));
                            $variantProduct->attributeValues()->sync(array_filter((array) $variant->attributes));
                        }
                    }

                    $variantProduct->load('attributeValues')->load('attributeValues.attribute');
                    $full_name = $variantProduct->name;
                    $variant_url = $variantProduct->url;
                    foreach ($variantProduct->attributeValues as $attributeValue){
                        $full_name .= ', ' . $attributeValue->attribute->name . ' ' . $attributeValue->value;
                        $variant_url .= '-' . $attributeValue->attribute->name . '-' . $attributeValue->value;
                    }
                    $variantProduct->full_name = $full_name;
                    $variantProduct->url = $this->validateURL($variant_url, $variantProduct->id);
                    $variantProduct->save();
                    //dump($variantProduct);
                }
            }
//dd('sss');
            DB::commit();
        } catch (\Exception $ex){dd($ex->getMessage(), $ex->getTrace());
            DB::rollBack();
            return redirect()->back()->with('fail', 'Ops, something went wrong!')->withErrors($validator)->withInput();
        }

        return redirect()->to('/admin/products')->with('success', 'Product updated!');
    }

    private function validateURL($name, $id = null)
    {
        $url = strtolower(str_replace(' ', '-', strip_tags($name)));
        $checkIfUnique = ProductTranslation::when($id, function ($query) use ($id){
                $query->where('product_id', $id);
            })
            ->where('locale', app()->getLocale())
            ->where('url', $url)
            ->first();
        $checkIfUniqueOnAll = ProductTranslation::where('locale', app()->getLocale())
            ->when($id, function ($query) use ($id){
                $query->where('product_id', '!=', $id);
            })
            ->where('url', $url)
            ->first();
        dump($name, $id, $checkIfUnique, $checkIfUniqueOnAll);
        if(!($checkIfUniqueOnAll->id ?? null) || ($checkIfUnique->product_id ?? null) == $id ){ dump($url); return urlencode($url); }

        return false;
    }

    public function loadProductAttributes(Request $request, $id = null)
    {
        $selectedProductAttributeValues = [];
        if($id){
            $product = Product::where('id', $id)->with('attributes')->with('attributes.attributeValues')->with('attributeValues')->first();
            $productAttributes = $product->attributes;
            $productAttributeValues = $product->attributeValues;
        }
        $attributes = Attribute::where('active', 1)->with('attributeValues')->get();
        if(($request->productAttributes ?? [])){
            if(count($request->productAttributes ?? [])) { $productAttributes = Attribute::whereIn('id', $request->productAttributes)->with('attributeValues')->get(); }
        }
        if(($request->productAttributeValues ?? [])){
            if(count($request->productAttributeValues ?? [])) {
                $selectedProductAttributeValues = AttributeValue::whereIn('id', $request->productAttributeValues)->with('attribute')->get();
                $selectedProductAttributeValues = $selectedProductAttributeValues->pluck('id')->toArray();
            }
        }

        if(count($selectedProductAttributeValues) < 1 && ($product->id ?? null)){
            $selectedProductAttributeValues = $product->attributeValues;
            $selectedProductAttributeValues = $selectedProductAttributeValues->pluck('id')->toArray();
        }

        $data = [
            'product' => ($product ?? null),
            'productAttributes' => ($productAttributes ?? []),
            'selectedProductAttributeValues' => ($selectedProductAttributeValues ?? []),
            'requestProductAttributeValues' => ($request->productAttributeValues ?? []),
            'attributes' => $attributes,
            'request' => $request,
        ];

        $productAttributes = view('os_views::admin.products.partials.list_attributes', $data)->render();

        return ['status' => 'success', 'productAttributes' => $productAttributes, 'request' => $request->all()];
    }

    public function loadProductVariants(Request $request, $id = null)
    {
        $variantIDs = [];
        $currentProduct = null;
        $products = [];
        $productAttributes = [];
        if(($request->variants[0] ?? null)){
            if(($request->variants[0]['attributes'] ?? null)){
                $productAttributes = Attribute::whereIn('id', array_keys($request->variants[0]['attributes']))->where('active', 1)->with('attributeValues')->get();
            }
        }

        if($id){
            $currentProduct = Product::where('id', $id)->with('attributes')->with('attributes.attributeValues')->with('attributeValues')->first();
            if($currentProduct->parent){
                $variantIDs = $currentProduct->parent->variants->pluck('id')->toArray();
                array_push($variantIDs, $currentProduct->parent->id);
            } else {
                $variantIDs = $currentProduct->variants->pluck('id')->toArray();
                array_push($variantIDs, $currentProduct->id);
            }

        }

        $products = Product::whereIn('id', $variantIDs)->get();

        if(($request->variants[0] ?? null)){
            foreach ($request->variants as $variant){
                $ch = Product::where('id', $variant['pId'])->first();
                if(!($ch->id ?? null)){
                    $unsavedProduct = new \stdClass();
                    $unsavedProduct->id = $variant['pId'];
                    $unsavedProduct->sku = ($variant['sku'] ?? '');
                    $unsavedProduct->name = ($variant['name'] ?? '');
                    $unsavedProduct->price = ($variant['price'] ?? 0);
                    $unsavedProduct->stock = ($variant['stock'] ?? 0);
                    $unsavedProduct->active = 1;
                    $unsavedProduct->unsaved = $unsavedProduct->id;
                    $products->push($unsavedProduct);
                }
            }
        }

        if(($request->addNew ?? null) == 1){
            $unsavedProduct = new \stdClass();
            $unsavedProduct->id = round(microtime(true) * 1000, 0);
            $unsavedProduct->sku = '';
            $unsavedProduct->name = '';
            $unsavedProduct->price = 0;
            $unsavedProduct->stock = 0;
            $unsavedProduct->active = 1;
            $unsavedProduct->unsaved = $unsavedProduct->id;
            $products->push($unsavedProduct);
        }

        foreach ($products as $product){
            if(($request->variants[0] ?? null)){
                foreach ($request->variants as $v){
                    if($v['pId'] == $product->id){
                        if(($v['attributes'] ?? null)) {
                            if(($product->unsaved ?? null)){
                                $product->selectedAttributes = array_keys($v['attributes']);
                                $product->selectedAttributeValues = array_unique($v['attributes']);
                            } else {
                                $product->setAttribute('selectedAttributes', array_keys($v['attributes']));
                                $product->setAttribute('selectedAttributeValues', $v['attributes']);
                            }
                        }
                    }
                }
            } else {
                $product->setAttribute('selectedAttributes', $product->attributes->pluck('id')->toArray());
                $product->setAttribute('selectedAttributeValues', $product->attributeValues->pluck('id')->toArray());
            }
        }

        if(count($productAttributes) < 1 && ($product->id)){
            $productAttributes = ($product->attributes ?? []);
        }

        $data = [
            'currentProduct' => ($currentProduct ?? null),
            'products' => $products,
            'productAttributes' => ($productAttributes ?? []),
            'request' => $request,
        ];

        $productVariantsView = view('os_views::admin.products.partials.list_product_variants', $data)->render();

        return ['status' => 'success', 'productVariantsView' => $productVariantsView, 'request' => $request->all()];
    }
}
