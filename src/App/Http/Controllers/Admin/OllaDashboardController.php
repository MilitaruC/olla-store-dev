<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class OllaDashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        return view('os_views::admin.dashboard.index');
    }
}
