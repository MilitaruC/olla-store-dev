<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Militaruc\OllaStore\App\Models\Attribute;
use Militaruc\OllaStore\App\Models\Category;
use Militaruc\OllaStore\App\Models\CategoryTranslation;
use HLP;

class OllaCategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $categories = Category::all();
        $json = '[{ "id": "0", "parent": "#", "text": "Store", data: {sort: 0} },';
        foreach($categories as $cat){
            $json .= '{ "id": "'.$cat->id.'", "parent": "'.($cat->parent_id ?? '0').'", "text": "'.$cat->name.'", data: {sort: '.($cat->sort ?? 999).'} },';
        }
        $json .= ']';
        //try{
        //    dump(hlp()->test());
        //} catch (\Exception $ex){
        //    dd($ex->getMessage(), $ex->getTrace());
        //}
        return view('os_views::admin.categories.index', ['json' => $json]);
    }

    public function ajaxList(Request $request)
    {
        $perPage = '10';
        $sortBy = 'categories.id';
        $sortOrder = 'desc';
        $search = ($request->search ?? '');
        $search = trim($search);
        $searches = explode(' ', $search);
        $active = '';

        if(($request->perPage ?? null)) $perPage = $request->perPage;
        if(($request->sortBy ?? null)) $sortBy = $request->sortBy;
        if(($request->sortOrder ?? null)) $sortOrder = $request->sortOrder;
        if(($request->activeStatus ?? null) == 'true' || ($request->activeStatus ?? null) == 'false') $active = $request->activeStatus;

        $categories = Category::with('translations')
            ->withCount('products')
            ->when(($active ?? null), function($query) use ($active) {
                return $query->where('active', '=', ($active == 'true' ? '1' : '0'));
            })
            ->when($request->search, function($query) use ($request, $searches){
                return $query->whereHas('translations', function ($query) use ($request, $searches){
                    foreach($searches as $key => $search) {
                        if($key == '0'){
                            $query->where('name', 'like', '%'.$search.'%')
                                ->orWhere('description', 'like', '%'.$search.'%');
                        }else{
                            $query->orWhere('name', 'like', '%'.$search.'%')
                                ->orWhere('description', 'like', '%'.$search.'%');
                        }
                    }
                });
            })
            ->orderBy($sortBy, $sortOrder)
            ->paginate($perPage);

        $data = [
            'categories' => $categories,
        ];

        return view('os_views::admin.categories.partials.list', $data)->render();
    }

    public function reorder(Request $request){
        DB::beginTransaction();
        try{
            foreach ($request->categoriesTreeSortOrder as $cat){
                $category = Category::where('id', $cat['id'])->first();
                $category->parent_id = $cat['parent_id'];
                $category->sort = $cat['sort'];
                $category->save();
            }
            DB::commit();
        } catch (\Exception $ex){
            DB::rollBack();
            return response()->json(['status' => 'fail', 'message' => $ex->getMessage()]);
        }
        return response()->json(['status' => 'success', 'message' => 'Succes']);
    }

    public function create()
    {
        $categories = Category::all();
        $json = '[{ "id": "0", "parent": "#", "text": "Store", data: {sort: 0} },';
        foreach($categories as $cat){
            $json .= '{ "id": "'.$cat->id.'", "parent": "'.($cat->parent_id ?? '0').'", "text": "'.$cat->name.'", data: {sort: '.($cat->sort ?? 999).'} },';
        }
        $json .= ']';

        return view('os_views::admin.categories.create', ['json' => $json]);
    }

    public function store(Request $request)
    {
        $url = ($request->url ?? $request->name ?? null);
        $validator = \Validator::make($request->all(),[
            'name' => ['required', 'min:3', 'max:191'],
            //'name' => ['required', 'unique:posts', 'max:255'],
            //'short_description' => ['required'],
        ]);
        $url = $this->validateURL($url);
        $validator->after(function ($validator) use ($url) {
            if (!$url) {
                $validator->errors()->add('url', 'Url or automatic url generation is not unique for current lang: '.app()->getLocale().'!');
            }
        });

        if ($validator->fails()) {
            return redirect('admin/categories/create')->withErrors($validator)->withInput();
        }
        DB::beginTransaction();
        try{
            $category = new Category();
            (($request->active ?? null) == 'on') ? $category->active = 1 : null;
            (($request->parent_id ?? 0) > 0) ? $category->parent_id = $request->parent_id : $category->parent_id = null;
            $category->name = $request->name;
            $category->short_description = $request->short_description;
            $category->description = $request->description;
            $category->url = $url;
            $category->meta_title = $request->meta_title;
            $category->meta_key = $request->meta_key;
            $category->meta_desc = $request->meta_desc;
            $category->save();

            DB::commit();
        } catch (\Exception $ex){dd($ex->getMessage(), $ex->getTrace());
            DB::rollBack();
            return redirect()->back()->with('fail', 'Ops, something went wrong!')->withErrors($validator)->withInput();
        }

        return redirect()->to('/admin/categories')->with('success', 'Category created!');
    }

    public function edit($id)
    {
        $categories = Category::all();
        $json = '[{ "id": "0", "parent": "#", "text": "Store", data: {sort: 0} },';
        foreach($categories as $cat){
            $json .= '{ "id": "'.$cat->id.'", "parent": "'.($cat->parent_id ?? '0').'", "text": "'.$cat->name.'", data: {sort: '.($cat->sort ?? 999).'} },';
        }
        $json .= ']';
        $category = Category::where('id', $id)->first();
        if(!($category->id ?? null)){ return redirect()->back()->with('fail', 'Ops, requested category not found!'); }

        return view('os_views::admin.categories.edit', ['category' => $category, 'json' => $json]);
    }

    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();
        $url = ($request->url ?? $request->name ?? null);
        $validator = \Validator::make($request->all(),[
            'name' => ['required', 'min:3', 'max:191'],
        ]);
        $url = $this->validateURL($url, $id);
        $validator->after(function ($validator) use ($url) {
            if (!$url) {
                $validator->errors()->add('url', 'Url or automatic url generation is not unique for current lang: '.app()->getLocale().'!');
            }
        });

        if ($validator->fails()) {
            return redirect('admin/categories/edit/'.$id)->withErrors($validator)->withInput();
        }
        DB::beginTransaction();
        try{
            (($request->active ?? null) == 'on') ? $category->active = 1 : null;
            (($request->parent_id ?? 0) > 0) ? $category->parent_id = $request->parent_id : $category->parent_id = null;
            $category->name = $request->name;
            $category->short_description = $request->short_description;
            $category->description = $request->description;
            $category->url = $url;
            $category->meta_title = $request->meta_title;
            $category->meta_key = $request->meta_key;
            $category->meta_desc = $request->meta_desc;
            $category->save();

            DB::commit();
        } catch (\Exception $ex){dd($ex->getMessage(), $ex->getTrace());
            DB::rollBack();
            return redirect()->back()->with('fail', 'Ops, something went wrong!')->withErrors($validator)->withInput();
        }

        return redirect()->to('/admin/categories')->with('success', 'Category updated!');
    }

    private function validateURL($name, $id = null)
    {
        $url = strtolower(str_replace(' ', '-', strip_tags($name)));
        $checkIfUnique = CategoryTranslation::when($id, function ($query) use ($id){
                $query->where('category_id', $id);
            })
            ->where('locale', app()->getLocale())
            ->where('url', $url)
            ->first();
        $checkIfUniqueOnAll = CategoryTranslation::where('locale', app()->getLocale())
            ->when($id, function ($query) use ($id){
                $query->where('category_id', '!=', $id);
            })
            ->where('url', $url)
            ->first();
        //dd($checkIfUnique, $checkIfUniqueOnAll);
        if(!($checkIfUniqueOnAll->id ?? null) || ($checkIfUnique->category_id ?? null) == $id ){ return $url; }

        return false;
    }

    public static function categoryTree($parent_id = null)
    {
        $categories = Category::where('parent_id', $parent_id)->get()->toArray();

        $tree = [];
        $i = 0;
        foreach($categories as $cat)
        {
            $tree[$i] = $cat;
            $tree[$i]['childen'] = self::categoryTree($cat['id']);
            $i ++;
        }
        return $tree;
    }

    public static function categoryParents($id)
    {
        $i = 0;
        $category = Category::where('id', $id)->first();
        $tree[$i] = $category;
        $cursor = $category;

        for($x = 10; $x >= 0; $x--)
        {
            $i++;
            $cursor = self::categoryParent(($cursor->parent_id ?? null));
            if(!($cursor->id ?? null)){ return $tree; }
            $tree[$i] = $cursor;
        }
        return $tree;
    }

    public static function categoryParent($parent_id = null)
    {
        $category = null;
        $parent_id ? $category = Category::where('id', $parent_id)->first() : null;
        return $category;
    }
}
