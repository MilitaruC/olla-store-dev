<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Militaruc\OllaStore\App\Models\Image as ProductImage;
use Militaruc\OllaStore\App\Models\Product;
Use Image;
use Intervention\Image\Exception\NotReadableException;

class OllaImageController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index()
    {

    }

    public function storeToProduct(Request $request, $productID)
    {
        $product = Product::where('id', $productID)->first();
        $parentID = $product->id;
        if ($product->configurable > 0) {
            $parentID = $product->parent->id ?? $product->id;
        }

        //dd($product, $request->all());
        $validatedData = $request->validate(['qqfile' => 'required|image|mimes:jpg,png,jpeg,webp|max:20000',]);
        $file = $request->qqfile;

        // for save original image
        $ogImage = Image::make($file);
        $relativePath = '/media/products/' . $parentID . '/';
        $originalPath = public_path($relativePath);
        File::ensureDirectoryExists($originalPath);
        $ogImage = $ogImage->save($originalPath . $file->getClientOriginalName());

        $ogImage->resize(800, 800);
        $thImage = $ogImage->save($originalPath . '800x800-' . $file->getClientOriginalName());
        $ogImage->resize(600, 600);
        $thImage = $ogImage->save($originalPath . '600x600-' . $file->getClientOriginalName());

        // for store resized image
        $thumbnailPath = public_path('/media/products/' . $parentID . '/thumbnail/');
        File::ensureDirectoryExists($thumbnailPath);
        $ogImage->resize(310, 310);
        $thImage = $ogImage->save($thumbnailPath . '310x310-' . $file->getClientOriginalName());
        $ogImage->resize(100, 100);
        $thImage = $ogImage->save($thumbnailPath . '100x100-' . $file->getClientOriginalName());
        $ogImage->resize(60, 60);
        $thImage = $ogImage->save($thumbnailPath . '60x60-' . $file->getClientOriginalName());
        $newImage = new ProductImage();
        $newImage->model = get_class($product);
        $newImage->file = $file->getClientOriginalName();
        $newImage->rel_path = $relativePath;
        $newImage->path = $originalPath;
        $newImage->save();

        if ($product->configurable > 0) {
            $newImage->products()->attach([$parentID]);
        } else {
            $newImage->products()->attach([$parentID => ['pid' => $parentID]]);
        }

        //return response()->json(['status' => 'success', 'message' => 'Image Has been uploaded successfully']);
        return response()->json(['success' => true]);
    }
}
