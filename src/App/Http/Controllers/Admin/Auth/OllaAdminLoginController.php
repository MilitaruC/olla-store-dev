<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OllaAdminLoginController extends Controller
{
    public function __construct(Request $request){//dd(auth()->guard());
        if(auth()->guard() != 'admin'){ return redirect()->route('olla-admin-login'); }
        $this->middleware('guest')->except('logout');
    }

    public function loginForm()
    {
        return view('os_views::admin.auth.login');
    }

    public function authenticate(Request $request)
    {//dd(auth()->guard('admin'));
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        try{
            if (Auth::guard('admin')->attempt($credentials)) {
                $request->session()->regenerate();

                return redirect()->intended('/admin');
            }
        } catch (\Exception $ex){
            dd($ex->getMessage(), $ex->getTrace());
        }


        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        //$request->session()->invalidate();
        foreach ($request->session()->all() as $session){
            if(is_string($session)) {
                if (strpos($session, 'admin_') !== false) {
                    $request->session()->forget($session);
                }
            }
        }

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
