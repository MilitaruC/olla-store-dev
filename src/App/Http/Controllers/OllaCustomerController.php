<?php

namespace Militaruc\OllaStore\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class OllaCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    public function index()
    {
        $data = [
            'cartData' => cart()->get(),
        ];
        return view('os_views::frontend.user.my-account', $data);
    }

    public function loadCartItemsAjax()
    {
        $cartData = cart()->get();

        $cartItemsView = view('os_views::frontend.cart.partials.items', ['cartData' => $cartData])->render();

        return Response()->json(['status' => 'success', 'cartItemsView' => $cartItemsView]);
    }

    public function checkout()
    {
        $data = [
            'cartData' => cart()->get(),
        ];
        return view('os_views::frontend.cart.checkout', $data);
    }

    public function addToCart(Request $request, $productID, $qty = 1)
    {
        $cartCookie = cart()->getCookie();
        $cart = Cart::where($cartCookie->cookie_name, $cartCookie->cookie_value)->where('product_id', $productID)->first();
        if(!($cart->id ?? null)){
            $cart = new Cart();
        }
        $cartCookie->cookie_name == 'customer_id' ? $cart->customer_id = $cartCookie->cookie_value : $cart->guest_id = $cartCookie->cookie_value;
        $cart->guest_id = null;
        $cart->product_id = $productID;
        $cart->product_qty = ($cart->product_qty ?? 0) + $qty;
        $cart->updated_at = date('Y-m-d h:i:m');
        $cart->save();

        $cartData = cart()->get();

        $data = [
            'status' => 'success',
            'cartData' => $cartData,
            'userCart' => $cartData->userCart,
            'cartTotalQty' => $cartData->userCart->sum('product_qty'),
        ];

        return Response()->json($data);
    }

    public function updateCart(Request $request, $productID, $qty = 1)
    {
        $cartCookie = cart()->getCookie();
        $cart = Cart::where($cartCookie->cookie_name, $cartCookie->cookie_value)->where('product_id', $productID)->first();
        if(!($cart->id ?? null)){
            $cart = new Cart();
        }
        $cartCookie->cookie_name == 'customer_id' ? $cart->customer_id = $cartCookie->cookie_value : $cart->guest_id = $cartCookie->cookie_value;
        $cart->guest_id = null;
        $cart->product_id = $productID;
        $cart->product_qty = $qty;
        $cart->updated_at = date('Y-m-d h:i:m');
        $cart->save();

        $cartData = cart()->get();

        $data = [
            'status' => 'success',
            'cartData' => $cartData,
            'userCart' => $cartData->userCart,
            'cartTotalQty' => $cartData->userCart->sum('product_qty'),
        ];

        return Response()->json($data);
    }

    public function removeItemFromCart(Request $request, $productID)
    {
        $cartCookie = cart()->getCookie();
        $cart = Cart::where($cartCookie->cookie_name, $cartCookie->cookie_value)->where('product_id', $productID)->first();
        if(!($cart->id ?? null)){
            $cartData = cart()->get();
            $data = [
                'status' => 'success',
                'cartData' => $cartData,
                'userCart' => $cartData->userCart,
                'cartTotalQty' => $cartData->userCart->sum('product_qty'),
            ];
            return Response()->json($data);
        }
        $cartCookie->cookie_name == 'customer_id' ? $cart->customer_id = $cartCookie->cookie_value : $cart->guest_id = $cartCookie->cookie_value;
        $cart->delete();

        $cartData = cart()->get();

        $data = [
            'status' => 'success',
            'cartData' => $cartData,
            'userCart' => $cartData->userCart,
            'cartTotalQty' => $cartData->userCart->sum('product_qty')
        ];

        return Response()->json($data);
    }
}
