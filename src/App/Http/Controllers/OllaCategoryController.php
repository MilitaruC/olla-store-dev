<?php

namespace Militaruc\OllaStore\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Militaruc\OllaStore\App\Models\Attribute;
use Militaruc\OllaStore\App\Models\Category;
use Militaruc\OllaStore\App\Models\Product;

class OllaCategoryController extends Controller
{
    public function index(Request $request, $slug)
    {
        $category = Category::with('translations')
            ->whereHas('translations', function ($query) use($slug){
                $query->where('url', $slug);
            })
            ->first();

        $perPage = '6';
        $sortBy = 'products.id';
        $sortOrder = 'asc';
        $search = ($request->search ?? '');
        $search = trim($search);
        $searches = explode(' ', $search);
        $active = '';
        $attributeValues = ($request->attributeValues ?? []);

        $products = Product::whereHas('categories', function ($query) use ($category){
                $query->where('id', $category->id);
            })
            ->when(count($attributeValues), function ($query) use ($attributeValues){
                $query->whereHas('attributeValues', function ($query) use ($attributeValues){
                    $query->whereIn('id', $attributeValues);
                });
            });
        $productsIds = $products;
        $productsIds = $productsIds->get()->pluck('id')->toArray();
        $attributes = Attribute::whereHas('products', function ($query) use ($productsIds){
                $query->whereIn('id', $productsIds);
            })
            ->with('attributeValues', function ($query) use ($productsIds){
                $query->whereHas('products', function ($query) use ($productsIds){
                    $query->whereIn('id', $productsIds);
                })->withCount('products');
            })
            ->get();

        $products = $products
            ->orderBy($sortBy, $sortOrder)
            ->paginate($perPage);

        $data = [
            'category' => $category,
            'attributes' => $attributes,
            'products' => $products,
            'breadcrumbs' => array_reverse(hlp()->categoryParents($category->id)),
            'cartData' => cart()->get(),
            'request' => $request,
        ];

        if($request->ajax()){
            return view('os_views::frontend.categories.partials.products-list', $data)->render();
        }

        return view('os_views::frontend.categories.index', $data);
    }
}
