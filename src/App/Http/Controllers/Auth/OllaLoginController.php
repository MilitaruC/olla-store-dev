<?php

namespace Militaruc\OllaStore\App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OllaLoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:web')->except('logout');
    }

    public function loginForm()
    {
        return view('os_views::frontend.auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        //if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
        //    $request->session()->regenerate();
        //
        //    return redirect()->intended('/');
        //}

        if (Auth::guard('web')->attempt($credentials)) {
            $request->session()->regenerate();
            //dd($request->all(), auth()->user());
            return redirect()->intended('/');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        //$request->session()->invalidate();
        foreach ($request->session()->all() as $session){
            if(is_string($session)){
                if(strpos($session, 'web_') !== false){
                    $request->session()->forget($session);
                }
            }

        }

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
