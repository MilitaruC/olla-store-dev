<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class AttributeValue extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['value','description'];

    public function attribute()
    {
        return $this->belongsTo(\Militaruc\OllaStore\App\Models\Attribute::class);
    }

    public function products()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Product::class);
    }
}
