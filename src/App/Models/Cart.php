<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('\Militaruc\OllaStore\App\Models\Product');
    }
}
