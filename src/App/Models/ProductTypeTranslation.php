<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTypeTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['name','description'];
    public $timestamps = true;
}
