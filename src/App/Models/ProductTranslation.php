<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['name','full_name','price','vat_price','price_without_vat','old_price','old_vat','old_price_without_vat','short_description','description','url','meta_title','meta_key','meta_desc'];
    public $timestamps = true;
}
