<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Store extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['store_url','robots_on','seo_title','meta_key','meta_desc','email','phone','company_data','address','open'];
}
