<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeValueTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['value','description'];
    public $timestamps = true;
}
