<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name','short_description','description','url','meta_title','meta_key','meta_desc'];

    public function parent()
    {
        return $this->belongsTo(\Militaruc\OllaStore\App\Models\Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(\Militaruc\OllaStore\App\Models\Category::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Product::class, 'category_product');
    }
}
