<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['name'];
    public $timestamps = true;
}
