<?php

namespace Militaruc\OllaStore\App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('\Militaruc\OllaStore\App\Models\Customer');
    }
}
