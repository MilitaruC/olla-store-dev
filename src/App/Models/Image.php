<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Image extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name'];

    public function products()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Product::class)->withPivot('pid');
    }

    public function getProductImagePathAttribute()
    {
        $path = $this->rel_path . $this->file;
        return $path;
    }
}
