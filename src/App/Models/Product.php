<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Product extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name','full_name','price','vat_price','price_without_vat','old_price','old_vat','old_price_without_vat','short_description','description','url','meta_title','meta_key','meta_desc'];

    public function categories()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Category::class, 'category_product');
    }

    public function attributes()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Attribute::class, 'attribute_product');
    }

    public function attributeValues()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\AttributeValue::class, 'attribute_value_product');
    }

    public function parent()
    {
        return $this->belongsTo(\Militaruc\OllaStore\App\Models\Product::class, 'has_parent', 'id');
    }

    public function variants()
    {
        return $this->hasMany(\Militaruc\OllaStore\App\Models\Product::class, 'has_parent', 'id');
    }

    public function images()
    {
        if ($this->configurable > 0) {
            return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Image::class, 'image_product', 'pid', 'image_id', 'id', 'id')
                ->withPivot('pid');
        } else {
            return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Image::class)->withPivot('pid');
        }
    }

    public function parentImages()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Image::class)->withPivot('pid');
    }
}
