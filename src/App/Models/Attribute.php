<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Attribute extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name','display_name'];

    public function attributeValues()
    {
        return $this->hasMany(\Militaruc\OllaStore\App\Models\AttributeValue::class);
    }

    public function products()
    {
        return $this->belongsToMany(\Militaruc\OllaStore\App\Models\Product::class);
    }
}
