<?php

namespace Militaruc\OllaStore\App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['store_url','robots_on','seo_title','meta_key','meta_desc','email','phone','company_data','address','open'];
    public $timestamps = true;
}
