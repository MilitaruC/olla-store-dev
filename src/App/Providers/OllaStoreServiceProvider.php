<?php

namespace Militaruc\OllaStore\App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Routing\Router;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Http\Kernel;
use Militaruc\OllaStore\App\Console\Commands\OllaPublish;
use Militaruc\OllaStore\App\Console\Commands\OllaSetup;
use Militaruc\OllaStore\App\Http\Middleware\Authenticate;

class OllaStoreServiceProvider extends ServiceProvider
{
    public function boot(Kernel $kernel)
    {
        Schema::defaultStringLength(191);

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'../../../routes/frontend.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        if ($this->app->runningInConsole()) {
            $this->commands([
                OllaSetup::class,
                OllaPublish::class,
            ]);
            $this->publishes([
                __DIR__.'/../../resources/assets' => public_path('os_assets'),
                __DIR__.'/../../config/olla.php' => config_path('olla.php'),
                __DIR__.'/../../config/auth.php' => config_path('auth.php'),
                __DIR__.'/../../config/translatable.php' => config_path('translatable.php'),
                __DIR__.'/../../routes/web.php' => base_path('routes/web.php'),
                __DIR__.'/../../routes/frontend.php' => base_path('routes/frontend.php'),
                __DIR__.'/../../routes/admin.php' => base_path('routes/admin.php'),
                __DIR__.'/../../resources/lang' => resource_path('lang'),
            ], 'assets');
        }

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'os_views');
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'os_langs');

        Paginator::defaultView('os_views::vendor.pagination.bootstrap-4');

        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', \Militaruc\OllaStore\App\Http\Middleware\Localization::class);
        $router->pushMiddlewareToGroup('admin', \App\Http\Middleware\EncryptCookies::class);
        $router->pushMiddlewareToGroup('admin', \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class);
        $router->pushMiddlewareToGroup('admin', \Illuminate\Session\Middleware\StartSession::class);
        $router->pushMiddlewareToGroup('admin', \Illuminate\View\Middleware\ShareErrorsFromSession::class);
        $router->pushMiddlewareToGroup('admin', \App\Http\Middleware\VerifyCsrfToken::class);
        $router->pushMiddlewareToGroup('admin', \Illuminate\Routing\Middleware\SubstituteBindings::class);
        //$router->pushMiddlewareToGroup('admin', \Militaruc\OllaStore\App\Http\Middleware\Authenticate::class);
        //$router->pushMiddlewareToGroup('admin', \Spatie\Permission\Middlewares\RoleMiddleware::class);
        //$router->pushMiddlewareToGroup('admin', \Spatie\Permission\Middlewares\PermissionMiddleware::class);
        //$router->pushMiddlewareToGroup('admin', \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class);




        // \Illuminate\Session\Middleware\AuthenticateSession::class,

        /*if(is_null(session('locale')))
        {
            session(['locale'=> "en"]);
        }
        app()->setLocale(session('locale'));

        public function change_lang($lang){
            if(in_array($lang,['en','tr','fa'])){
                session(['locale'=> $lang]);
            }
            return back();
        }*/

    }

    public function register()
    {
        //$this->app->singleton('hlp', function ($app) {
        //    return new HLP();
        //});
        //$this->app->bind('hlp', function($app) {
        //    return new HLP();
        //});
    }
}
