<?php

namespace Militaruc\OllaStore\App\Support;

use Militaruc\OllaStore\App\Models\Category;

class HLP
{

    public function getVendorPath()
    {
        return str_replace('/App/Support', '',  __DIR__);
    }

    public function getVendorRelativePath()
    {
        return str_replace(base_path(), '', str_replace('/App/Support', '',  __DIR__));
    }

    public function getVendorName()
    {
        $path = str_replace(base_path(), '', str_replace('/src/App/Support', '',  __DIR__));
        $pathChunks = explode('/', $path);
        return end($pathChunks);
    }

    /**
     * @param $src
     * @param $dst
     * @param $force
     * @return void
     */
    public static function copy_directory($src, $dst, $force = false)
    {
        // open the source directory
        $dir = opendir($src);
        // Make the destination directory if not exist
        @mkdir($dst);
        // Loop through the files in source directory
        while( $file = readdir($dir) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) )
                {
                    if (!file_exists($dst)) {  mkdir($dst, 0775, true); }
                    // subdirectory
                    self::copy_directory($src . '/' . $file, $dst . '/' . $file, $force);
                }
                else {
                    if(!file_exists($dst.'/'.$file) || $force){  copy($src . '/' . $file, $dst . '/' . $file); }
                }
            }
        }
        closedir($dir);
    }

    public function copyDirectory($src, $dst, $force = false)
    {
        self::copy_directory($src, $dst, $force = false);
    }

    public function test()
    {
        dd(get_class_methods(HLP::class));
    }

    public static function categoryTree($parent_id = null)
    {
        $categories = Category::where('parent_id', $parent_id)->get();

        $tree = [];
        $i = 0;
        foreach($categories as $cat)
        {
            $tree[$i] = $cat;
            $tree[$i]->setAttribute('children', self::categoryTree($cat->id));
            $i ++;
        }
        return $tree;
    }

    public static function categoryParents($id)
    {
        $i = 0;
        $category = Category::where('id', $id)->first();
        $tree[$i] = $category;
        $cursor = $category;

        for($x = 10; $x >= 0; $x--)
        {
            $i++;
            $cursor = self::categoryParent(($cursor->parent_id ?? null));
            if(!($cursor->id ?? null)){ return $tree; }
            $tree[$i] = $cursor;
        }
        return $tree;
    }

    public static function categoryParent($parent_id = null)
    {
        $category = null;
        $parent_id ? $category = Category::where('id', $parent_id)->first() : null;
        return $category;
    }

    public static function jsTreeCategories($selectedIDs = [])
    {
        $categories = Category::all();
        $json = '[{ "id": "0", "parent": "#", "text": "Store", state : { selected : false, opened : true }, data: {sort: 0} },';
        foreach($categories as $cat){
            in_array($cat->id, $selectedIDs) ? $selected = 'state : { selected : true, opened : true }, ' : $selected = 'state : { selected : false, opened : true }, ';
            $json .= '{ "id": "'.$cat->id.'", "parent": "'.($cat->parent_id ?? '0').'", "text": "'.$cat->name.'", '.$selected.'data: {sort: '.($cat->sort ?? 999).'} },';
        }
        $json .= ']';

        return $json;
    }

    public static function categoryBreadcrumb($id)
    {
        $i = 0;
        $category = Category::where('id', $id)->first();
        $tree[$i] = $category;
        $cursor = $category;

        for($x = 10; $x >= 0; $x--)
        {
            $i++;
            $cursor = self::categoryBreadcrumb(($cursor->parent_id ?? null));
            if(!($cursor->id ?? null)){ return $tree; }
            $tree[$i] = $cursor;
        }
        return $tree;
    }

    public static function percentDif($old, $new) {
        if($old < 1 || $new < 1) return 0;
        $diff = number_format(((1 - $old / $new) * 100),2,',','');
        return $diff;
    }

    public static function vatFromGross($priceGross, $vatPercent) {
        $vatValue = $priceGross - ($priceGross / (1 + ($vatPercent / 100)));
        return number_format($vatValue, 10, '.','');
    }

    public static function priceWithoutVatFromGross($priceGross, $vatPercent) {
        $vatValue = $priceGross - ($priceGross / (1 + ($vatPercent / 100)));
        $priceWithoutVat = $priceGross - $vatValue;
        return number_format($priceWithoutVat, 10, '.','');
    }

}
