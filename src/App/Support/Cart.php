<?php

namespace Militaruc\OllaStore\App\Support;

use Illuminate\Support\Facades\Auth;
use Militaruc\OllaStore\App\Models\Cart as CartModel;

class Cart {

    public function getCookie()
    {
        $cartCookie = new \stdClass();
        $cartCookie->cookie_name = '';
        $cartCookie->cookie_value = '';
        $user = (Auth::user() ?? null);
        if(($user->id ?? null)) {
            if (!($_COOKIE['customer_id'] ?? null)) {
                $customerID = base64_encode($user->id);
                setcookie('customer_id', $customerID, time() + (86400 * 30), "/"); // 86400 = 1 day
                $cartCookie->cookie_name = 'customer_id';
                $cartCookie->cookie_value = $customerID;
                return $cartCookie;
            } elseif (base64_decode($_COOKIE['customer_id'] ?? '') != $user->id){
                $customerID = base64_encode($user->id);
                setcookie('customer_id', $customerID, time() + (86400 * 30), "/"); // 86400 = 1 day
                $cartCookie->cookie_name = 'customer_id';
                $cartCookie->cookie_value = $customerID;
                return $cartCookie;
            }
            $cartCookie->cookie_name = 'customer_id';
            $cartCookie->cookie_value = base64_decode($_COOKIE['customer_id']);
            return $cartCookie;
        } else {
            if(!($_COOKIE['guest_id'] ?? null)) {
                $milliseconds = floor(microtime(true) * 1000);
                setcookie('guest_id', $milliseconds, time() + (86400 * 30), "/"); // 86400 = 1 day
                $cartCookie->cookie_name = 'guest_id';
                $cartCookie->cookie_value = $milliseconds;
                return $cartCookie;
            }
            $cartCookie->cookie_name = 'guest_id';
            $cartCookie->cookie_value = $_COOKIE['guest_id'];
            return $cartCookie;
        }
    }

    public function get($returnJson = false)
    {
        $cartCookie = $this->getCookie();
        $userCart = CartModel::where($cartCookie->cookie_name, $cartCookie->cookie_value)->with('product')->with('product.images')->get();
        $cartData = new \stdClass();
        $cartData->userCart = $userCart;
        $cartData->cartTotalQty = $userCart->sum('product_qty');
        $cartData->productsPrice = 0;
        $cartData->deliveryPrice = 10;
        $cartData->totalPrice = 0;
        foreach ($userCart as $cart){
            $cartData->productsPrice += ($cart->product->price * $cart->product_qty);
        }
        $cartData->totalPrice = round(($cartData->productsPrice + $cartData->deliveryPrice), 2);

        if($returnJson){
            return Response()->json($cartData);
        }

        return $cartData;
    }
}
