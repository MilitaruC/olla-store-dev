<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(file_exists(base_path('routes/frontend.php'))){ include(base_path('routes/frontend.php')); }
else{ include(base_path('vendor/militaruc/'.olla()->getVendorName().'/src/routes/frontend.php')); }

if(file_exists(base_path('routes/admin.php'))){ include(base_path('routes/admin.php')); }
else{ include(base_path('vendor/militaruc/'.olla()->getVendorName().'/src/routes/admin.php')); }


