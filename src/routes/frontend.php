<?php

use Militaruc\OllaStore\App\Http\Controllers\Auth\OllaLoginController;
 use Militaruc\OllaStore\App\Http\Controllers\OllaCustomerController;
use Militaruc\OllaStore\App\Http\Controllers\OllaHomeController;
use Militaruc\OllaStore\App\Http\Controllers\OllaCategoryController;
use Militaruc\OllaStore\App\Http\Controllers\OllaProductController;
use Militaruc\OllaStore\App\Http\Controllers\OllaCartController;

Route::group(['middleware' => ['web']], function () {
    Route::get('/login', [OllaLoginController::class, 'loginForm'])->name('olla-login-form');
    Route::post('/login', [OllaLoginController::class, 'authenticate'])->name('olla-authenticate');
    Route::get('/logout', [OllaLoginController::class, 'logout'])->name('olla-logout');

    Route::group(['middleware' => ['auth']], function () {
         Route::get('/my-account', [OllaCustomerController::class, 'index'])->name('olla-my-account');
    });

    Route::get('/', [OllaHomeController::class, 'index'])->name('olla-home');

    Route::get('/c/{id}', [OllaCategoryController::class, 'index'])->name('olla-category');
    Route::post('/c/{id}', [OllaCategoryController::class, 'index'])->name('olla-category-ajax');

    Route::get('/p/{id}', [OllaProductController::class, 'index'])->name('olla-product');

    Route::get('/cart', [OllaCartController::class, 'index'])->name('olla-cart-index');
    Route::get('/cart/checkout', [OllaCartController::class, 'checkout'])->name('olla-checkout');
    Route::get('/addToCart/{productID}/{qty}', [OllaCartController::class, 'addToCart'])->name('olla-addToCart');
    Route::get('/updateCart/{productID}/{qty}', [OllaCartController::class, 'updateCart'])->name('olla-updateCart');
    Route::get('/removeItemFromCart/{productID}', [OllaCartController::class, 'removeItemFromCart'])->name('olla-removeItemFromCart');
    Route::get('/loadCartItemsAjax', [OllaCartController::class, 'loadCartItemsAjax'])->name('olla-loadCartItemsAjax');
});



