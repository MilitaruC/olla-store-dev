<?php

use Militaruc\OllaStore\App\Http\Controllers\Admin\Auth\OllaAdminLoginController;
use Militaruc\OllaStore\App\Http\Controllers\Admin\OllaDashboardController;
use Militaruc\OllaStore\App\Http\Controllers\Admin\OllaCategoryController;
use Militaruc\OllaStore\App\Http\Controllers\Admin\OllaProductController;
use Militaruc\OllaStore\App\Http\Controllers\Admin\OllaImageController;

Route::group(['guard' => ['admin'], 'middleware' => ['admin'], 'prefix' => 'admin'], function () {

    Route::get('/language/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    });

    Route::get('/login', [OllaAdminLoginController::class, 'loginForm'])->name('olla-admin-login');
    Route::post('/login', [OllaAdminLoginController::class, 'authenticate'])->name('olla-admin-authenticate');

    Route::group(['middleware' => ['auth:admin']], function () {

        Route::get('/logout', [OllaAdminLoginController::class, 'logout'])->name('olla-admin-logout');

        Route::get('/', [OllaDashboardController::class, 'index'])->name('olla-admin-dashboard');

        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', [OllaCategoryController::class, 'index'])->name('olla-admin-categories-index');
            Route::post('/ajax-list', [OllaCategoryController::class, 'ajaxList'])->name('olla-admin-categories-ajax-list');
            Route::post('/reorder', [OllaCategoryController::class, 'reorder'])->name('olla-admin-categories-reorder');
            Route::get('/create', [OllaCategoryController::class, 'create'])->name('olla-admin-categories-create');
            Route::post('/store', [OllaCategoryController::class, 'store'])->name('olla-admin-categories-store');
            Route::get('/edit/{id}', [OllaCategoryController::class, 'edit'])->name('olla-admin-categories-edit');
            Route::post('/update/{id}', [OllaCategoryController::class, 'update'])->name('olla-admin-categories-update');
        });

        Route::group(['prefix' => 'products'], function () {
            Route::get('/', [OllaProductController::class, 'index'])->name('olla-admin-products-index');
            Route::post('/ajax-list', [OllaProductController::class, 'ajaxList'])->name('olla-admin-products-ajax-list');
            Route::get('/create', [OllaProductController::class, 'create'])->name('olla-admin-products-create');
            Route::post('/store', [OllaProductController::class, 'store'])->name('olla-admin-products-store');
            Route::get('/edit/{id}', [OllaProductController::class, 'edit'])->name('olla-admin-products-edit');
            Route::post('/update/{id}', [OllaProductController::class, 'update'])->name('olla-admin-products-update');
            Route::get('/loadProductAttributes/{id?}', [OllaProductController::class, 'loadProductAttributes'])->name('olla-admin-products-loadProductAttributes');
            Route::post('/loadProductVariants/{id?}', [OllaProductController::class, 'loadProductVariants'])->name('olla-admin-products-loadProductVariants');
        });

        Route::group(['prefix' => 'images'], function () {
            Route::post('/ajax-store-to-product/{id}', [OllaImageController::class, 'storeToProduct'])->name('olla-images-admin-storeToProduct');
        });

    });
});

