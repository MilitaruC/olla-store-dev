<?php

return [
    'product_types' => [
        'physical' => 'Physical',
        'digital' => 'Digital',
    ],
    'address_types' => [
        'person' => 'Person',
        'company' => 'Company',
        'ong' => 'ONG',
        'delivery' => 'Delivery',
    ],
    'delivery_methods' => [
        'personal_lifting' => 'Personal Lifting',
        'courier' => 'Courier',
    ],
    'payment_methods' => [
        'on_delivery' => 'On Delivery',
        'on_location' => 'On Location',
        'online' => 'Online',
    ],
];
