<?php

if(!function_exists('hlp')){
    function hlp()
    {
        return new \Militaruc\OllaStore\App\Support\HLP();
    }
}

if(!function_exists('cart')){
    function cart()
    {
        return new \Militaruc\OllaStore\App\Support\Cart();
    }
}

if(!function_exists('olla')){
    function olla()
    {
        return new \Militaruc\OllaStore\App\Support\HLP();
    }
}

if(!function_exists('pageTitle')){
    function pageTitle($title)
    {
        return view('os_views::frontend.partials.page-title', ['title' => $title]);
    }
}

if (!function_exists('percentDif')) {
    function percentDif($old, $new) {
        if($old < 1 || $new < 1) return 0;
        $diff = number_format(((1 - $old / $new) * 100),2,',','');
        return $diff;
    }
}

if (!function_exists('vatFromGross')) {
    function vatFromGross($priceGross, $vatPercent) {
        $vatValue = $priceGross - ($priceGross / (1 + ($vatPercent / 100)));
        return number_format($vatValue, 10, '.','');
    }
}

if (!function_exists('priceWithoutVatFromGross')) {
    function priceWithoutVatFromGross($priceGross, $vatPercent) {
        $vatValue = $priceGross - ($priceGross / (1 + ($vatPercent / 100)));
        $priceWithoutVat = $priceGross - $vatValue;
        return number_format($priceWithoutVat, 10, '.','');
    }
}

