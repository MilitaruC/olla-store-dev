<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('locale')->index();

            // Foreign key to the main model
            $table->unsignedBigInteger('product_id');
            $table->unique(['product_id', 'locale']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            // Actual fields you want to translate
            $table->string('name');
            $table->string('full_name');

            $table->decimal('price', 15, 4);
            $table->decimal('vat_price', 15, 4)->nullable()->default(null);
            $table->decimal('price_without_vat', 15, 4)->nullable()->default(null);
            $table->decimal('old_price', 15, 4)->nullable()->default(null);
            $table->decimal('old_vat', 15, 4)->nullable()->default(null);
            $table->decimal('old_price_without_vat', 15, 4)->nullable()->default(null);

            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->string('url');
            $table->string('meta_title')->nullable();
            $table->string('meta_key')->nullable();
            $table->text('meta_desc')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
    }
}
