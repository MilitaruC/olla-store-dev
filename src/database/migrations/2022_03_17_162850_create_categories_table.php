<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable()->default(null);
            $table->integer('depth')->default(0);
            $table->boolean('active')->default(0);
            $table->integer('sort')->default(999);
            $table->boolean('demo')->default(0);
            //$table->unsignedBigInteger('depth_1')->nullable()->default(null);
            //$table->unsignedBigInteger('depth_2')->nullable()->default(null);
            //$table->unsignedBigInteger('depth_3')->nullable()->default(null);
            //$table->unsignedBigInteger('depth_4')->nullable()->default(null);
            //$table->unsignedBigInteger('depth_5')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
