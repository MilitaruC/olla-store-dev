<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('locale')->index();

            // Foreign key to the main model
            $table->unsignedBigInteger('store_id');
            $table->unique(['store_id', 'locale']);
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');

            // Actual fields you want to translate
            $table->string('store_url')->unique();
            $table->boolean('robots_on')->default(0);
            $table->string('seo_title');
            $table->string('meta_key');
            $table->text('meta_desc')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->text('company_data')->nullable();
            $table->text('address')->nullable();
            $table->text('open')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
