<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vendor_id')->nullable()->default(1);
            $table->unsignedBigInteger('type_id');
            $table->boolean('configurable')->default(0);
            $table->unsignedBigInteger('unit_id')->default(0);
            $table->integer('min_quantity')->default(1);
            $table->unsignedBigInteger('manufacturer_id')->default(0);
            $table->unsignedBigInteger('brand_id')->default(0);
            $table->string('sku')->default(0);
            $table->integer('stock')->nullable()->default(0);
            $table->boolean('active')->default(0);
            $table->boolean('on_demand')->default(0);
            $table->boolean('is_parent')->default(1);
            $table->unsignedBigInteger('has_parent')->nullable()->default(null);
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->timestamps();

            $table->unique(['sku','vendor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
