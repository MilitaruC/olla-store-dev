<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'name' => 'Test Customer1',
            'email' => 'test@customer1.com',
            'email_verified_at' => '2022-03-10',
            'password' => bcrypt('P@ss4321'),
        ]);

        Customer::create([
            'name' => 'Test Customer2',
            'email' => 'test@customer2.com',
            'email_verified_at' => '2022-03-10',
            'password' => bcrypt('P@ss4321'),
        ]);
    }
}
