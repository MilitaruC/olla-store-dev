<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\ProductType;
use Militaruc\OllaStore\App\Models\ProductTypeTranslation;

class ProductTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productType = new ProductType();
        $productType->active = 1;
        $productType->save();

        $productTypeTranslation = new ProductTypeTranslation();
        $productTypeTranslation->locale = 'en';
        $productTypeTranslation->product_type_id = $productType->id;
        $productTypeTranslation->name = 'Physical';
        $productTypeTranslation->description = 'Physical product';
        $productTypeTranslation->save();

        $productTypeTranslation = new ProductTypeTranslation();
        $productTypeTranslation->locale = 'ro';
        $productTypeTranslation->product_type_id = $productType->id;
        $productTypeTranslation->name = 'Produs fizic';
        $productTypeTranslation->description = 'Produs fizic';
        $productTypeTranslation->save();

        $productType = new ProductType();
        $productType->active = 1;
        $productType->save();

        $productTypeTranslation = new ProductTypeTranslation();
        $productTypeTranslation->locale = 'en';
        $productTypeTranslation->product_type_id = $productType->id;
        $productTypeTranslation->name = 'Digital';
        $productTypeTranslation->description = 'Digital products';
        $productTypeTranslation->save();

        $productTypeTranslation = new ProductTypeTranslation();
        $productTypeTranslation->locale = 'ro';
        $productTypeTranslation->product_type_id = $productType->id;
        $productTypeTranslation->name = 'Digital';
        $productTypeTranslation->description = 'Produse digitale';
        $productTypeTranslation->save();

        //$productType = new ProductType();
        //$productType->active = 1;
        //$productType->save();
        //
        //$productTypeTranslation = new ProductTypeTranslation();
        //$productTypeTranslation->locale = 'en';
        //$productTypeTranslation->product_type_id = $productType->id;
        //$productTypeTranslation->name = 'Access';
        //$productTypeTranslation->description = 'Access products';
        //$productTypeTranslation->save();
        //
        //$productTypeTranslation = new ProductTypeTranslation();
        //$productTypeTranslation->locale = 'ro';
        //$productTypeTranslation->product_type_id = $productType->id;
        //$productTypeTranslation->name = 'Acces';
        //$productTypeTranslation->description = 'Acces';
        //$productTypeTranslation->save();

    }
}
