<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\Admin;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin();
        $admin->name = 'Admin';
        $admin->email = 'admin@admin.com';
        $admin->email_verified_at = '2022-03-10';
        $admin->password = bcrypt('P@ss4321');
        $admin->save();

        $role = new Role();
        $role->name = 'Admin';
        $role->guard_name = 'admin';
        $role->save();

        $admin->assignRole('Admin');
    }
}
