<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\Attribute;
use Militaruc\OllaStore\App\Models\AttributeTranslation;
use Militaruc\OllaStore\App\Models\AttributeValue;
use Militaruc\OllaStore\App\Models\AttributeValueTranslation;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Color';
        $attributeTranslation->display_name = 'Color';
        $attributeTranslation->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'ro';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Culoare';
        $attributeTranslation->display_name = 'Culoare';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'White';
        $attributeValueTranslation->description = 'White';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Alb';
        $attributeValueTranslation->description = 'Alb';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Black';
        $attributeValueTranslation->description = 'Black';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Negru';
        $attributeValueTranslation->description = 'Negru';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Red';
        $attributeValueTranslation->description = 'Red';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Rosu';
        $attributeValueTranslation->description = 'Rosu';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Size';
        $attributeTranslation->display_name = 'Size';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'S';
        $attributeValueTranslation->description = 'Small';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'S';
        $attributeValueTranslation->description = 'Mic';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'M';
        $attributeValueTranslation->description = 'Medium';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'M';
        $attributeValueTranslation->description = 'Mediu';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'L';
        $attributeValueTranslation->description = 'Large';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'L';
        $attributeValueTranslation->description = 'Mare';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'XL';
        $attributeValueTranslation->description = 'Extra Large';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'XL';
        $attributeValueTranslation->description = 'Extra Mare';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Material';
        $attributeTranslation->display_name = 'Material';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Cotton';
        $attributeValueTranslation->description = 'Cotton';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Bumbac';
        $attributeValueTranslation->description = 'Bumbac';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Plastic';
        $attributeValueTranslation->description = 'Plastic';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Plastic';
        $attributeValueTranslation->description = 'Plastic';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Season';
        $attributeTranslation->display_name = 'Season';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Summer';
        $attributeValueTranslation->description = 'Summer';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Vara';
        $attributeValueTranslation->description = 'Vara';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Winter';
        $attributeValueTranslation->description = 'Winter';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Iarna';
        $attributeValueTranslation->description = 'Iarna';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Style';
        $attributeTranslation->display_name = 'Style';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Casual';
        $attributeValueTranslation->description = 'Casual';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Casual';
        $attributeValueTranslation->description = 'Casual';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Elegant';
        $attributeValueTranslation->description = 'Elegant';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Elegant';
        $attributeValueTranslation->description = 'Elegant';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Model';
        $attributeTranslation->display_name = 'Model';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Classic';
        $attributeValueTranslation->description = 'Classic';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Clasic';
        $attributeValueTranslation->description = 'Clasic';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Polo';
        $attributeValueTranslation->description = 'Polo';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Polo';
        $attributeValueTranslation->description = 'Polo';
        $attributeValueTranslation->save();

        $attribute = new Attribute();
        $attribute->active = 1;
        $attribute->type = 'generic';
        $attribute->display_type = 'checkbox';
        $attribute->save();

        $attributeTranslation = new AttributeTranslation();
        $attributeTranslation->locale = 'en';
        $attributeTranslation->attribute_id = $attribute->id;
        $attributeTranslation->name = 'Cut';
        $attributeTranslation->display_name = 'Cut';
        $attributeTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Tall';
        $attributeValueTranslation->description = 'Tall';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Tall';
        $attributeValueTranslation->description = 'Tall';
        $attributeValueTranslation->save();

        $attributeValue = new AttributeValue();
        $attributeValue->attribute_id = $attribute->id;
        $attributeValue->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'en';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Petite';
        $attributeValueTranslation->description = 'Petite';
        $attributeValueTranslation->save();

        $attributeValueTranslation = new AttributeValueTranslation();
        $attributeValueTranslation->locale = 'ro';
        $attributeValueTranslation->attribute_value_id = $attributeValue->id;
        $attributeValueTranslation->value = 'Petite';
        $attributeValueTranslation->description = 'Petite';
        $attributeValueTranslation->save();

    }
}
