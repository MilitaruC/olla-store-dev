<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\Category;
use Militaruc\OllaStore\App\Models\CategoryTranslation;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        $category = new Category();
        $category->active = 1;
        $category->sort = 0;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Women';
        $categoryTranslation->short_description = 'Women Short Description';
        $categoryTranslation->description = 'Women Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Femei';
        $categoryTranslation->short_description = 'Femei Short Description';
        $categoryTranslation->description = 'Femei Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 2
        $category = new Category();
        $category->active = 1;
        $category->sort = 6;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Plus Size Clothing';
        $categoryTranslation->short_description = 'Plus Size Clothing Short Description';
        $categoryTranslation->description = 'Plus Size Clothing Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Marimi Mari';
        $categoryTranslation->short_description = 'Plus Size Clothing Short Description';
        $categoryTranslation->description = 'Plus Size Clothing Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 3
        $category = new Category();
        $category->active = 1;
        $category->sort = 5;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Shirts / Tops';
        $categoryTranslation->short_description = 'Shirts / Tops Short Description';
        $categoryTranslation->description = 'Shirts / Tops Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Tricouri / Maiouri';
        $categoryTranslation->short_description = 'Tricouri / Maiouri Short Description';
        $categoryTranslation->description = 'Tricouri / Maiouri Clothing Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 4
        $category = new Category();
        $category->active = 1;
        $category->sort = 4;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Dresses & Skirts';
        $categoryTranslation->short_description = 'Dresses & Skirts Short Description';
        $categoryTranslation->description = 'Dresses & Skirts Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Rochii & Fuste';
        $categoryTranslation->short_description = 'Rochii & Fuste Short Description';
        $categoryTranslation->description = 'Rochii & Fuste Clothing Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 5
        $category = new Category();
        $category->active = 1;
        $category->sort = 3;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Sweaters & Hoodie';
        $categoryTranslation->short_description = 'Sweaters & Hoodie Short Description';
        $categoryTranslation->description = 'Sweaters & Hoodie Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Sweaters & Hoodie ro';
        $categoryTranslation->short_description = 'Sweaters & Hoodie Short Description';
        $categoryTranslation->description = 'Sweaters & Hoodie Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 6
        $category = new Category();
        $category->active = 1;
        $category->sort = 2;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Trousers & Legging Set';
        $categoryTranslation->short_description = 'Trousers & Legging Set Short Description';
        $categoryTranslation->description = 'Trousers & Legging Set Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Trousers & Legging Set ro';
        $categoryTranslation->short_description = 'Trousers & Legging Set Short Description';
        $categoryTranslation->description = 'Trousers & Legging Set Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 7
        $category = new Category();
        $category->active = 1;
        $category->sort = 1;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Shoes';
        $categoryTranslation->short_description = 'Shoes Short Description';
        $categoryTranslation->description = 'Shoes Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Pantofi';
        $categoryTranslation->short_description = 'Pantofi Short Description';
        $categoryTranslation->description = 'Pantofi Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 8
        $category = new Category();
        $category->active = 1;
        $category->sort = 0;
        $category->parent_id = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Underwear & Socks';
        $categoryTranslation->short_description = 'Underwear & Socks Description';
        $categoryTranslation->description = 'Underwear & Socks Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Chiloti & Sosete';
        $categoryTranslation->short_description = 'Chiloti & Sosete Short Description';
        $categoryTranslation->description = 'Chiloti & Sosete Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 9
        $category = new Category();
        $category->active = 1;
        $category->sort = 1;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Men';
        $categoryTranslation->short_description = 'Men Description';
        $categoryTranslation->description = 'Men Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Barbati';
        $categoryTranslation->short_description = 'Barbati Short Description';
        $categoryTranslation->description = 'Barbati Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 10
        $category = new Category();
        $category->active = 1;
        $category->sort = 4;
        $category->parent_id = 9;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Shirts / T-Shirts';
        $categoryTranslation->short_description = 'Shirts / T-Shirts Description';
        $categoryTranslation->description = 'Shirts / T-Shirts Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Tricouri / Shorturi';
        $categoryTranslation->short_description = 'Tricouri / Shorturi Short Description';
        $categoryTranslation->description = 'Tricouri / Shorturi Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 11
        $category = new Category();
        $category->active = 1;
        $category->sort = 3;
        $category->parent_id = 9;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Jackets';
        $categoryTranslation->short_description = 'Jackets Description';
        $categoryTranslation->description = 'Jackets Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Jachete';
        $categoryTranslation->short_description = 'Jachete Short Description';
        $categoryTranslation->description = 'Jachete Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 12
        $category = new Category();
        $category->active = 1;
        $category->sort = 2;
        $category->parent_id = 9;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Hoodies';
        $categoryTranslation->short_description = 'Hoodies Description';
        $categoryTranslation->description = 'Hoodies Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Hanorace';
        $categoryTranslation->short_description = 'Hanorace Short Description';
        $categoryTranslation->description = 'Hanorace Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 13
        $category = new Category();
        $category->active = 1;
        $category->sort = 1;
        $category->parent_id = 9;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Shoes';
        $categoryTranslation->short_description = 'Shoes Description';
        $categoryTranslation->description = 'Shoes Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Pantofi';
        $categoryTranslation->short_description = 'Pantofi Short Description';
        $categoryTranslation->description = 'Pantofi Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();
        // 14
        $category = new Category();
        $category->active = 1;
        $category->sort = 0;
        $category->parent_id = 9;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Underwear & Socks';
        $categoryTranslation->short_description = 'Underwear & Socks Description';
        $categoryTranslation->description = 'Underwear & Socks Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Chiloti & Shosete';
        $categoryTranslation->short_description = 'Chiloti & Shosete Short Description';
        $categoryTranslation->description = 'Chiloti & Shosete Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();
        // 15
        $category = new Category();
        $category->active = 1;
        $category->sort = 2;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Kids';
        $categoryTranslation->short_description = 'Kids Description';
        $categoryTranslation->description = 'Kids Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Copii';
        $categoryTranslation->short_description = 'Copii Short Description';
        $categoryTranslation->description = 'Copii Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 16
        $category = new Category();
        $category->active = 1;
        $category->sort = 3;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Accessories';
        $categoryTranslation->short_description = 'Accessories Description';
        $categoryTranslation->description = 'Accessories Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Accesorii';
        $categoryTranslation->short_description = 'Accesorii Short Description';
        $categoryTranslation->description = 'Accesorii Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();
        // 17
        $category = new Category();
        $category->active = 1;
        $category->sort = 1;
        $category->parent_id = 16;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Men';
        $categoryTranslation->short_description = 'Men Description';
        $categoryTranslation->description = 'Men Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Barbati';
        $categoryTranslation->short_description = 'Barbati Short Description';
        $categoryTranslation->description = 'Barbati Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-men';
        $categoryTranslation->save();
        // 18
        $category = new Category();
        $category->active = 1;
        $category->sort = 0;
        $category->parent_id = 16;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Women';
        $categoryTranslation->short_description = 'Women Description';
        $categoryTranslation->description = 'Women Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-women';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Femei';
        $categoryTranslation->short_description = 'Femei Short Description';
        $categoryTranslation->description = 'Femei Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-femei';
        $categoryTranslation->save();
        // 19
        $category = new Category();
        $category->active = 1;
        $category->sort = 4;
        $category->save();
        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'en';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Sale';
        $categoryTranslation->short_description = 'Sale Description';
        $categoryTranslation->description = 'Sale Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-en';
        $categoryTranslation->save();

        $categoryTranslation = new CategoryTranslation();
        $categoryTranslation->locale = 'ro';
        $categoryTranslation->category_id = $category->id;
        $categoryTranslation->name = 'Reduceri';
        $categoryTranslation->short_description = 'Reduceri Short Description';
        $categoryTranslation->description = 'Reduceri Description';
        $categoryTranslation->url = strtolower(str_replace('/', '', str_replace(' ', '-', $categoryTranslation->name))) . '-ro';
        $categoryTranslation->save();

    }
}
