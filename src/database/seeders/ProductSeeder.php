<?php

namespace Militaruc\OllaStore\Database\Seeders;

use Illuminate\Database\Seeder;
use Militaruc\OllaStore\App\Models\Product;
use Militaruc\OllaStore\App\Models\ProductTranslation;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product();
        $product->vendor_id = 1;
        $product->type_id = 1;
        $product->min_quantity = 1;
        $product->sku = 'IM26FJ4196002';
        $product->stock = 1247;
        $product->active = 1;
        $product->is_parent = 1;
        $product->save();

        $productTranslation = new ProductTranslation();
        $productTranslation->locale = 'en';
        $productTranslation->product_id = $product->id;
        $productTranslation->name = 'CHIFFON TWIST 2';
        $productTranslation->full_name = 'CHIFFON TWIST 2';
        $productTranslation->price = 52;
        $productTranslation->vat_price = 8.3025;
        $productTranslation->price_without_vat = 43.6975;
        $productTranslation->old_price = 62;
        $productTranslation->old_vat = 9.8992;
        $productTranslation->old_price_without_vat = 52.1008;
        $productTranslation->short_description = '<p>This cardigan has a slim fit, button up front and round neckline.

Finely-pleated CHIFFON TWIST 2 tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.</p>';
        $productTranslation->description = '<p>This cardigan has a slim fit, button up front and round neckline.

Finely-pleated CHIFFON TWIST 2 tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.</p>';
        $productTranslation->url = 'chiffon-twist-2';
        $productTranslation->meta_title = 'CHIFFON TWIST 2';
        $productTranslation->meta_key = 'chiffon, twist';
        $productTranslation->meta_desc = 'Finely-pleated CHIFFON TWIST 2 tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.';
        $productTranslation->save();

        $product->categories()->sync([1,3,16,18]);

        $product = new Product();
        $product->vendor_id = 1;
        $product->type_id = 1;
        $product->min_quantity = 1;
        $product->sku = 'IM26FJ419600FLAT';
        $product->stock = 263;
        $product->active = 1;
        $product->is_parent = 1;
        $product->save();

        $productTranslation = new ProductTranslation();
        $productTranslation->locale = 'en';
        $productTranslation->product_id = $product->id;
        $productTranslation->name = 'CHIFFON TWIST FLAT';
        $productTranslation->full_name = 'CHIFFON TWIST FLAT';
        $productTranslation->price = 52;
        $productTranslation->vat_price = 8.3025;
        $productTranslation->price_without_vat = 43.6975;
        $productTranslation->old_price = 62;
        $productTranslation->old_vat = 9.8992;
        $productTranslation->old_price_without_vat = 52.1008;
        $productTranslation->short_description = '<p>This cardigan has a slim fit, button up front and round neckline.

Finely-pleated CHIFFON TWIST FLAT tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.</p>';
        $productTranslation->description = '<p>This cardigan has a slim fit, button up front and round neckline.

Finely-pleated CHIFFON TWIST FLAT tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.</p>';
        $productTranslation->url = 'chiffon-twist-flat';
        $productTranslation->meta_title = 'CHIFFON TWIST FLAT';
        $productTranslation->meta_key = 'chiffon, twist';
        $productTranslation->meta_desc = 'Finely-pleated CHIFFON TWIST FLAT tops are constructed from sheer fabric in seasonal colours and are ideal for layering. The polyester thread is now made from 100% recycled material.';
        $productTranslation->save();

        $product->categories()->sync([1,3,16,18]);

    }
}
