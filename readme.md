# Olla Store

---------------

### Description

------

This is a [Laravel](https://laravel.com/) package intended to create an online store boilerplate.

### Installation & Setup
1. Have a fresh installation of [Laravel 8](https://laravel.com/docs/8.x)

`composer create-project --prefer-dist laravel/laravel blog "8.*"`

2. Install Olla Store package ```composer require militaruc/olla-store```
3. Set your ```.env``` database connection credentials
4. Artisan commands

- `php artisan olla:setup` Follow the setup steps
- `php artisan olla:publish` Gives access to Config, Controllers, Models and other to modify or use it to extend with custom functionalities. Be aware not to let the files published if you want to benefit from future update. It is better to use them just for extend.

5. asd
6. 




